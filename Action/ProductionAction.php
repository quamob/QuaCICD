<?php

namespace Kanboard\Plugin\QuaCICD\Action;

use Kanboard\Model\TaskModel;
use Kanboard\Action\Base;
use Kanboard\Plugin\QuaCICD\Helper\HelperQuaCICD;

/**
 * ProductionAction
 *
 * @package action
 * @author  Alexandre Turpin
 */
class ProductionAction extends Base
{
    /**
     * Get automatic action description
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return t('Execute production deployment script when moving a task in a specific column');
    }

    /**
     * Get the list of compatible events
     *
     * @access public
     * @return array
     */
    public function getCompatibleEvents()
    {
        return array(
            TaskModel::EVENT_MOVE_COLUMN,
        );
    }

    /**
     * Get the required parameter for the action (defined by the user)
     *
     * @access public
     * @return array
     */
    public function getActionRequiredParameters()
    {
        return array(
            'column_id' => t('Column'),
        );
    }

    /**
     * Get the required parameter for the event
     *
     * @access public
     * @return string[]
     */
    public function getEventRequiredParameters()
    {
        return array(
            'project_id',
            'task_id',
            'column_id',
            'src_column_id',
        );
    }

    /**
     * Execute the action
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool            True if the action was executed or false when not executed
     */
    public function doAction(array $data)
    {
        if($data['column_id'] == $this->getParam('column_id') && $data['src_column_id'] == $this->columnModel->getColumnIdByTitle($data['project_id'], $this->projectQuaCICDModel->getByProjectId($data['project_id'])['qualification_colmun_name'])){          
            
            $scriptDeployPath = HelperQuaCICD::getPathScripts($this->projectModel->getByIdWithOwner($data['project_id'])['owner_username'], $data['project_id'], $this->projectModel->getById($data['project_id'])['name']) . "Deploy_in_production.sh";
            $scriptMergePath = HelperQuaCICD::getPathScripts($this->projectModel->getByIdWithOwner($data['project_id'])['owner_username'], $data['project_id'], $this->projectModel->getById($data['project_id'])['name']) . "Merge_Branch.sh";


            $parametersForScript = HelperQuaCICD::getMergeParameters($this->projectModel->getByIdWithOwner($data['project_id'])['owner_username'], $data['project_id'], HelperQuaCICD::formatProjectName($this->projectModel->getById($data['project_id'])['name']), "Task/" . $data['task_id'] . "-" . HelperQuaCICD::formatTaskTitle($this->taskFinderModel->getById($data['task_id'])['title']), "master", $this->configModel->getOption('quaCICD_merge_squash_option'));

            if(is_dir(HelperQuaCICD::getPathGitRepository($this->projectModel->getByIdWithOwner($data['project_id'])['owner_username'], $data['project_id'], HelperQuaCICD::formatProjectName($this->projectModel->getById($data['project_id'])['name']))) && is_dir(HelperQuaCICD::getPathEnvironment($this->projectModel->getByIdWithOwner($data['project_id'])['owner_username'], $data['project_id'], HelperQuaCICD::formatProjectName($this->projectModel->getById($data['project_id'])['name'])))){
                exec($scriptMergePath . " " . $parametersForScript . " 2>&1", $outputMerge, $returnMerge);
                exec($scriptDeployPath . " " . HelperQuaCICD::getPathEnvironment($this->projectModel->getByIdWithOwner($data['project_id'])['owner_username'], $data['project_id'], HelperQuaCICD::formatProjectName($this->projectModel->getById($data['project_id'])['name'])) . " 2>&1", $outputDeploy, $returnDeploy);
            }
            else
            {
                $this->flash->failure(t("One of the environement doesn't exist"));
                $this->taskPositionModel->movePosition($data['project_id'], $data['task_id'], $data['src_column_id'], 1, 0, false);
                return false;
            }

            if($returnMerge)
            {
                $this->flash->failure(t("Automatic merge failed. You need to merge manually. Go to the task comment section for more informations"));

                $commentValue = array('task_id' => $data['task_id'], 
                                'user_id' => $this->userSession->getId(), 
                                'date_creation' => time(), 
                                'comment' => t("Automatic merge failed. You need to merge manually.") . "\n\n\n- git checkout " . "Task/" . $data['task_id'] . "-" . HelperQuaCICD::formatTaskTitle($this->taskFinderModel->getById($data['task_id'])['title']) . "\n- git merge master\n\n" . t("Resolve conflicts") . "\n\n" . t("Once there is no more conflict, push your modifications : ") . "\n+ git add . \n+ git commit -m '" . t('Your commit message') . "' \n+ git push \n\n" . t("Finally, you can move the task in the Kanban."),
                                'reference' => '', 
                                'date_modification' => time());
            
                $this->commentModel->create($commentValue);

                $this->taskPositionModel->movePosition($data['project_id'], $data['task_id'], $data['src_column_id'], 1, 0, false);
                return false;
            }

            if($returnDeploy)
            {
                $this->flash->failure(t("Automatic deployment in production failed. ") . implode($outputDeploy));

                $this->taskPositionModel->movePosition($data['project_id'], $data['task_id'], $data['src_column_id'], 1, 0, false);
                return false;
            }

            $this->flash->success(t("Production deployment script successfully launched"));

            $commentValue = array('task_id' => $data['task_id'], 
                                'user_id' => $this->userSession->getId(), 
                                'date_creation' => time(), 
                                'comment' => t("Production deployment successfully done"), 
                                'reference' => '', 
                                'date_modification' => time());
            
            $this->commentModel->create($commentValue);

            return $this->taskModificationModel->update(array('id' => $data['task_id']));
        }
        else if($data['column_id'] == $this->getParam('column_id')){
            $this->flash->failure(t("You cannot move to this task to this column"));
            $this->taskPositionModel->movePosition($data['project_id'], $data['task_id'], $data['src_column_id'], 1, 0, false);
            return false;
        }
        else if($data['src_column_id'] == $this->getParam('column_id'))
        {
            $this->flash->failure(t("You cannot move to this task to this column"));
            $this->taskPositionModel->movePosition($data['project_id'], $data['task_id'], $data['src_column_id'], 1, 0, false);
            return false;
        }
        return false;
    }

    /**
     * Check if the event data meet the action condition
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool
     */
    public function hasRequiredCondition(array $data)
    {
        return isset($data['project_id'], $data['task_id'], $data['column_id'], $data['src_column_id']);
    }
}