<?php

namespace Kanboard\Plugin\QuaCICD\Action;

use Kanboard\Model\TaskModel;
use Kanboard\Action\Base;
use Kanboard\Plugin\QuaCICD\Helper\HelperQuaCICD;
use Kanboard\Core\Plugin\Loader;
use Kanboard\Plugin\QuaBDD\Model\TaskQuaBDDGherkinModel;

/**
 * BranchCreationAction
 *
 * @package action
 * @author  Alexandre Turpin
 */
class BranchCreationAction extends Base
{
    /**
     * Get automatic action description
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return t('Create a new branch in the git repository while moving to a specific column');
    }

    /**
     * Get the list of compatible events
     *
     * @access public
     * @return array
     */
    public function getCompatibleEvents()
    {
        return array(
            TaskModel::EVENT_MOVE_COLUMN,
        );
    }

    /**
     * Get the required parameter for the action (defined by the user)
     *
     * @access public
     * @return array
     */
    public function getActionRequiredParameters()
    {
        return array(
            'column_id' => t('Column'),
        );
    }

    /**
     * Get the required parameter for the event
     *
     * @access public
     * @return string[]
     */
    public function getEventRequiredParameters()
    {
        return array(
            'project_id',
            'task_id',
            'column_id',
            'src_column_id',
        );
    }

    /**
     * Execute the action
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool            True if the action was executed or false when not executed
     */
    public function doAction(array $data)
    {
        if($data['column_id'] == $this->getParam('column_id') && ($data['src_column_id'] == $this->columnModel->getColumnIdByTitle($data['project_id'], t('Backlog')) || $data['src_column_id'] == $this->columnModel->getColumnIdByTitle($data['project_id'], t('Ready')))){            
            
            $tempFolderForbranchCreation = "/srv/" . QUACICD_SUBDIR_ENV . "/Branch Creation";
            if (file_exists($tempFolderForbranchCreation)) { unlink($tempFolderForbranchCreation); }
            mkdir($tempFolderForbranchCreation);
            if (is_dir($tempFolderForbranchCreation)) {
                chdir($tempFolderForbranchCreation);
                exec('git clone ' . HelperQuaCICD::getPathGitRepository($this->projectModel->getByIdWithOwner($data['project_id'])['owner_username'], $data['project_id'], $this->projectModel->getById($data['project_id'])['name']) . ' Development_Environment 2>&1', $output, $return);
                chdir($tempFolderForbranchCreation. "/Development_Environment");
                exec('git config user.email "dev@locahost"', $output, $return);
                exec('git config user.name "Dossier Dev"', $output, $return);
                exec('git branch -a 2>&1', $branchs, $return);
                $branchExist = in_array("  remotes/origin/" . "Task/". $data['task_id'] . "-"  . HelperQuaCICD::formatTaskTitle($this->taskFinderModel->getById($data['task_id'])['title']), $branchs);
                if(!$branchExist){
                    exec('git checkout master 2>&1', $output, $return);
                    exec('git checkout -b "' . "Task/". $data['task_id'] . "-"  . HelperQuaCICD::formatTaskTitle($this->taskFinderModel->getById($data['task_id'])['title']) .'"'. " 2>&1", $output, $return);
                    exec('git push --set-upstream origin "'. "Task/". $data['task_id'] . "-"  . HelperQuaCICD::formatTaskTitle($this->taskFinderModel->getById($data['task_id'])['title']) . '"'. " 2>&1", $output, $return);
                    exec('chmod -R a+w ' . HelperQuaCICD::getPathGitRepository($this->projectModel->getByIdWithOwner($data['project_id'])['owner_username'], $data['project_id'], $this->projectModel->getById($data['project_id'])['name']));
                }

                if(HelperQuaCICD::scanTestQuaBDDQuaCICD())
                {
                    $needToPush = false;
                    exec('git checkout "' . "Task/". $data['task_id'] . "-"  . HelperQuaCICD::formatTaskTitle($this->taskFinderModel->getById($data['task_id'])['title']) .'"'. " 2>&1", $output, $return);

                    $taskQuaBDDGherkinModel = new TaskQuaBDDGherkinModel($this->container);
                    $allGherkin = $taskQuaBDDGherkinModel->getAllGherkinByTaskId($data['task_id']);

                    if($allGherkin){
                        if(!is_dir($tempFolderForbranchCreation. "/Development_Environment/features")){
                            exec('mkdir -m777 features', $output, $return);
                            $needToPush = true;
                        }
                        chdir($tempFolderForbranchCreation. "/Development_Environment/features");
                        $task = $this->taskFinderModel->getById($data['task_id']);
                        if(!file_exists($tempFolderForbranchCreation. "/Development_Environment/features/" . HelperQuaCICD::formatQuaBDDFeatureFile($task['title']) . ".feature")){   
                            $task = $this->taskFinderModel->getById($data['task_id']);
                            $featureFile = fopen(HelperQuaCICD::formatQuaBDDFeatureFile($task['title']) . ".feature", "w");
                            
                            $txt = "Feature: " . $task['title'] . "\n";
                            fwrite($featureFile, $txt);
                            $txt = "    " . str_replace("\n", "\n    ", $task['description']) . "\n";
                            fwrite($featureFile, $txt);

                            foreach($allGherkin as $gherkin){
                                $givens = json_decode($gherkin['given']);
                                $whens = json_decode($gherkin['when']);
                                $thens = json_decode($gherkin['then']);

                                $txt = "\n    Scenario: " . $gherkin['title'] . "\n";
                                fwrite($featureFile, $txt);

                                if(isset($givens[0])){
                                    $txt = "        Given " . $givens[0] . "\n";
                                    fwrite($featureFile, $txt);
                                    unset($givens[0]);
                                }
                                foreach($givens as $given){
                                    $txt = "        And " . $given . "\n";
                                    fwrite($featureFile, $txt);            
                                }

                                if(isset($whens[0])){
                                    $txt = "        When " . $whens[0] . "\n";
                                    fwrite($featureFile, $txt);
                                    unset($whens[0]);
                                }
                                foreach($whens as $when){
                                    $txt = "        And " . $when . "\n";
                                    fwrite($featureFile, $txt);            
                                }

                                if(isset($thens[0])){
                                    $txt = "        Then " . $thens[0] . "\n";
                                    fwrite($featureFile, $txt);
                                    unset($thens[0]);
                                }
                                foreach($thens as $then){
                                    $txt = "        And " . $then . "\n";
                                    fwrite($featureFile, $txt);            
                                }
                            }

                            fclose($featureFile);
                            chmod(HelperQuaCICD::formatQuaBDDFeatureFile($task['title']) . ".feature", 0777);
                            $needToPush = true;
                        }

                        if(!is_dir($tempFolderForbranchCreation. "/Development_Environment/steps")){
                            exec('mkdir -m777 steps', $output, $return);
                            $needToPush = true;
                        }
                        chdir($tempFolderForbranchCreation. "/Development_Environment/features/steps");
                        
                        if(!file_exists($tempFolderForbranchCreation. "/Development_Environment/features/steps/" . HelperQuaCICD::formatQuaBDDFeatureFile($task['title']) . "_step.py")){  
                            $task = $this->taskFinderModel->getById($data['task_id']); 
                            $stepFile = fopen(HelperQuaCICD::formatQuaBDDFeatureFile($task['title']) . "_step.py", "w");
                            
                            $txt = "from behave import *\n\n";
                            fwrite($stepFile, $txt); 
                            
                            $givens = array();
                            $whens = array();
                            $thens = array();

                            foreach($allGherkin as $gherkin){
                                $givenGherkins = json_decode($gherkin['given']);
                                $whensGherkins = json_decode($gherkin['when']); 
                                $thensGherkins = json_decode($gherkin['then']);

                                foreach($givenGherkins as $givenGherkin){
                                    $givens[] = $givenGherkin;
                                }

                                foreach($whensGherkins as $whensGherkin){
                                    $whens[] = $whensGherkin;
                                }

                                foreach($thensGherkins as $thensGherkin){
                                    $thens[] = $thensGherkin;
                                }

                                $givens = array_unique($givens);
                                $whens = array_unique($whens);
                                $thens = array_unique($thens);
                            }

                            foreach($givens as $given){
                                $txt = "\n" . '@Given("' . $given . '")';
                                fwrite($stepFile, $txt); 

                                $txt = "\n" . 'def step_impl(context):';
                                fwrite($stepFile, $txt);

                                $txt = "\n" . '  raise NotImplementedError("STEP: Given ' . $given . '")' . "\n";
                                fwrite($stepFile, $txt);
                            }

                            foreach($whens as $when){
                                $txt = "\n" . '@When("' . $when . '")';
                                fwrite($stepFile, $txt); 

                                $txt = "\n" . 'def step_impl(context):';
                                fwrite($stepFile, $txt);

                                $txt = "\n" . '  raise NotImplementedError("STEP: When ' . $when . '")' . "\n";
                                fwrite($stepFile, $txt);
                            }

                            foreach($thens as $then){
                                $txt = "\n" . '@Then("' . $then . '")';
                                fwrite($stepFile, $txt); 

                                $txt = "\n" . 'def step_impl(context):';
                                fwrite($stepFile, $txt);

                                $txt = "\n" . '  raise NotImplementedError("STEP: Then ' . $then . '")' . "\n";
                                fwrite($stepFile, $txt);
                            }
                            

                            fclose($stepFile);
                            chmod(HelperQuaCICD::formatQuaBDDFeatureFile($task['title']) . "_step.py", 0777);
                            $needToPush = true;
                        }

                    }

                    if($needToPush){
                        chdir($tempFolderForbranchCreation. "/Development_Environment");
                        exec('git config user.email "dev@locahost"', $output, $return);
                        exec('git config user.name "Dossier Dev"', $output, $return);
                        exec('git add . 2>&1', $output, $return);
                        exec('git commit -m "Adding gherkin to project" 2>&1', $output, $return);
                        exec('git push 2>&1', $output, $return);
                    }
                }

                chdir("/srv");
                $this->delete_files($tempFolderForbranchCreation . '/');
            }

            if($return)
            {
                $this->flash->failure(t("Error during the branch creation script : ") . implode($output));
                $this->taskPositionModel->movePosition($data['project_id'], $data['task_id'], $data['src_column_id'], 1, 0, false);
                return false;
            }

            if(!$branchExist){
                $this->flash->success(t("Git branch ") . "Task/". $data['task_id'] . "-"  . HelperQuaCICD::formatTaskTitle($this->taskFinderModel->getById($data['task_id'])['title']) . t(" successfully created"));
                
                $commentValue = array('task_id' => $data['task_id'], 
                                    'user_id' => $this->userSession->getId(), 
                                    'date_creation' => time(), 
                                    'comment' => t("Git branch ") . "Task/". $data['task_id'] . "-"  . HelperQuaCICD::formatTaskTitle($this->taskFinderModel->getById($data['task_id'])['title']) . t(" successfully created"), 
                                    'reference' => '', 
                                    'date_modification' => time());
                
                $this->commentModel->create($commentValue);
            }
            else
            {
                $this->flash->success(t("Git branch ") . "Task/". $data['task_id'] . "-"  . HelperQuaCICD::formatTaskTitle($this->taskFinderModel->getById($data['task_id'])['title']) . t(" already exist"));
                
                $commentValue = array('task_id' => $data['task_id'], 
                                    'user_id' => $this->userSession->getId(), 
                                    'date_creation' => time(), 
                                    'comment' => t("Git branch ") . "Task/". $data['task_id'] . "-"  . HelperQuaCICD::formatTaskTitle($this->taskFinderModel->getById($data['task_id'])['title']) . t(" already exist"), 
                                    'reference' => '', 
                                    'date_modification' => time());
                
                $this->commentModel->create($commentValue);
            }
            
            return $this->taskModificationModel->update(array('id' => $data['task_id']));
        }
        else if($data['src_column_id'] == $this->getParam('column_id') && ($data['column_id'] == $this->columnModel->getColumnIdByTitle($data['project_id'], t('Ready')) || $data['column_id'] == $this->columnModel->getColumnIdByTitle($data['project_id'], t('Backlog'))))
        {
            $this->flash->failure(t("You cannot move to this task to this column"));
            $this->taskPositionModel->movePosition($data['project_id'], $data['task_id'], $data['src_column_id'], 1, 0, false);
            return false;
        }

        if($data['src_column_id'] == $this->columnModel->getColumnIdByTitle($data['project_id'], t('Done')) && ($data['column_id'] == $this->columnModel->getColumnIdByTitle($data['project_id'], t('Ready')) || $data['column_id'] == $this->columnModel->getColumnIdByTitle($data['project_id'], t('Backlog'))))
        {
            $this->flash->failure(t("You cannot move to this task to this column"));
            $this->taskPositionModel->movePosition($data['project_id'], $data['task_id'], $data['src_column_id'], 1, 0, false);
            return false;
        }

        if($data['column_id'] == $this->columnModel->getColumnIdByTitle($data['project_id'], t('Done')) && ($data['src_column_id'] == $this->columnModel->getColumnIdByTitle($data['project_id'], t('Ready')) || $data['src_column_id'] == $this->columnModel->getColumnIdByTitle($data['project_id'], t('Backlog'))))
        {
            $this->flash->failure(t("You cannot move to this task to this column"));
            $this->taskPositionModel->movePosition($data['project_id'], $data['task_id'], $data['src_column_id'], 1, 0, false);
            return false;
        }

        return false;
    }

    /**
     * Check if the event data meet the action condition
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool
     */
    public function hasRequiredCondition(array $data)
    {
        return isset($data['project_id'], $data['task_id'], $data['column_id'], $data['src_column_id']);
    }

    public function delete_files($dir) {
        if (is_dir($dir)) { 
            $objects = scandir($dir);
            foreach ($objects as $object) { 
                if ($object != "." && $object != "..") { 
                    if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
                        BranchCreationAction::delete_files($dir. DIRECTORY_SEPARATOR .$object);
                    else
                        unlink($dir. DIRECTORY_SEPARATOR .$object); 
                } 
            }
            rmdir($dir); 
        }
        elseif(is_file($dir)) {
            unlink( $dir );  
        }
    }
}
