<?php use Kanboard\Plugin\QuaCICD\Helper\HelperQuaCICD; ?>
<?php if($this->quaCICDProjectHelper->isQuaCICDProject($project['id'])): ?>
    <div id="quaCICD-git-clone">
        <i class="fa fa-git-square fa-lg" aria-hidden="true"></i>
        <input id="quaCICD-git-clone-input" type="text" value="git clone <?= HelperQuaCICD::getPathGitRepository($this->text->e($project['owner_username']), $project['id'], $project['name']) ?>" readonly></input>
    </div>
<?php endif?>