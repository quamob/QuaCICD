QuaCICD
==============================

This plugin allow you to execute automated deployment in qualification and production directly on your board.

Author
------

- Alexandre Turpin
- Quamob
- License MIT

Requirements
------------

- Kanboard >= 1.2.14
- Git
- Linux command : 'wc'
- Write permission for user 'www-data' on : `/srv`
- Write permission for user 'www-data' on : `/var/www/html`

Installation
------------

You have the choice between 3 methods:

1. Install the plugin from the Kanboard plugin manager in one click
2. Download the zip file and decompress everything under the directory `plugins/QuaCICD`
3. Clone this repository into the folder `plugins/QuaCICD`

Note: Plugin folder is case-sensitive.

Security Disclaimer
------------

This is the first version of the plugin. It is still in development, it could have some security issues.

Documentation
------------

When you create a QuaCICD Project it will :

- On Kanboard :
    - Create 2 new columns (By default : Qualification and Production)
    - Attach 3 automatic actions on the columns 'Work in Progress', 'Qualification', 'Production'

- On your file system :
    - Create Git repository and scripts repository in `/srv`
    - Create the Production and the Qualification environments in `/var/www/html`

**Utilisation**

1. Create a task
2. Move the task to the 'Work in Progress' column : it will create a git branch named 'Task/ID-TASKNAME'
3. Do every modification you need into this branch and push them
4. Move the task to the qualification column : It wil merge the task branch into the qualification branch and pull the modification in the qualification repository.
5. Test your new functionality
6. If you are satified with it move to 7. otherwise move the task to the 'Work in Progress' column and go back to 3.
7. Move the task to the production column : It wil merge the task branch into master branch and pull the modification in the production repository.

Moreover, you will have some logs in the comment section of tasks like the date and time of the branch creation or the production deployment.

**Resolving merge conflicts**

If you have some merge conflicts during the qualification or production deloypment, the deployment will be canceled.
Then you will have some instructions to recreate the conflicts and resolve them in the comment section of the task.

**Change deployments methods**

You can change the deployment methods by change changing the differents scripts in the scripts repository of the project in /srv.

**How to clone your project git repository**

If you are working directly locally you can simply copy/paste the git clone command that is written on your Kanboard board.
If you are using SSH you will have to use this command :

    git clone SSH_USER@SERVER_IP:/srv/QuaCICD/../PATH_TO_THE_GIT_REPOSITORY/../.git

If you are using a different SSH port you have to specify it :

    git clone ssh://SSH_USER@SERVER_IP:[SERVER_PORT]/srv/QuaCICD/../PATH_TO_THE_GIT_REPOSITORY/../.git

Moreover, make sure that you have the writing write on the git repository directory. Otherwise, you will not be able to execute git command to you git repository.

**Options**

There are two options that you can change directly in the database. The first option 'quaboard_merge_squash_option' is a merge option. The option is set to false by default because the merge squash option is not available yet because it generates conflicts. The second option is 'quaboard_delete_branch_in_production'. If it is true , it allow you to delete branch after merging in master (production), if it is false the branch will remain avec the deployment in production.

**Association with [QuaBDD](https://gitlab.com/quamob/QuaBDD) plugin**

If you have installed QuaBDD plugin, you have acces to another feature. When you move a task in the 'Work in Progress' column and your task has gherkin in it. It will create the .feature and .step directly in the branch that is created by the task. Those files will be stored in a 'features' folder.
