<?php

namespace Kanboard\Plugin\QuaCICD\Schema;

use PDO;

const VERSION = 1;

function version_1(PDO $pdo)
{
    $pdo->exec("
        CREATE TABLE `quaCICD_project` (
            id INTEGER PRIMARY KEY,
            project_id INTEGER NOT NULL UNIQUE,
            qualification_colmun_name TEXT NOT NULL,
            production_colmun_name TEXT NOT NULL,
            FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE
        )
    ");
}