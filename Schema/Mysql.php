<?php

namespace Kanboard\Plugin\QuaCICD\Schema;

use PDO;

const VERSION = 1;

function version_1(PDO $pdo)
{
    $pdo->exec("
        CREATE TABLE `quaCICD_project` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `project_id` INT NOT NULL UNIQUE,
            `qualification_colmun_name` VARCHAR(255) NOT NULL,
            `production_colmun_name` VARCHAR(255) NOT NULL,
            FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE,
            PRIMARY KEY(id)
        ) ENGINE=InnoDB CHARSET=utf8
    ");
}