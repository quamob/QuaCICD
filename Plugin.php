<?php

namespace Kanboard\Plugin\QuaCICD;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;
use Kanboard\Plugin\QuaCICD\Model\ProjectQuaCICDModel;
use Kanboard\Plugin\QuaCICD\Model\TaskQuaCICDGherkinModel;
use Kanboard\Plugin\QuaCICD\Action\ProductionAction;
use Kanboard\Plugin\QuaCICD\Action\QualificationAction;
use Kanboard\Plugin\QuaCICD\Action\BranchCreationAction;

class Plugin extends Base
{

    public function initialize()
    {
        defined('QUACICD_SUBDIR_ENV') or define('QUACICD_SUBDIR_ENV', getenv('QUACICD_SUBDIR_ENV') ?: 'QuaCICD');

        //Default values
        $this->configModel->getOption('quaCICD_default_production_name') !== '' ? : $values['quaCICD_default_production_name'] = 'Production';
        $this->configModel->getOption('quaCICD_default_qualification_name') !== '' ? : $values['quaCICD_default_qualification_name'] = 'Qualification';
        $this->configModel->getOption('quaCICD_merge_squash_option') !== '' ? : $values['quaCICD_merge_squash_option'] = 'false';
        $this->configModel->getOption('quaCICD_delete_branch_in_production') !== '' ? : $values['quaCICD_delete_branch_in_production'] = 'true';

        if(isset($values)){
            $this->configModel->save($values);
        }

        //CSS
        $this->hook->on('template:layout:css', array('template' => 'plugins/QuaCICD/Asset/quaCICD.css'));
        
        //JS
        $this->hook->on('template:layout:js', array('template' => 'plugins/QuaCICD/Asset/quaCICD.js'));

        //Template
        $this->template->hook->attach('template:dashboard:page-header:menu', 'quaCICD:project/newQuaCICDProject');  
        $this->template->hook->attach('template:project-list:menu:after', 'quaCICD:project/newQuaCICDProject');  
        $this->template->setTemplateOverride('board/table_container', 'quaCICD:board/table_container');
        $this->template->setTemplateOverride('comment/show', 'quaCICD:comment/show');
        $this->template->hook->attach('template:project:header:after', 'quaCICD:board/GitCloneBoard');

        //Model
        $this->container['projectQuaCICDModel'] = $this->container->factory(function ($c) {return new ProjectQuaCICDModel ($c);});

        //Action
        $this->actionManager->register(new ProductionAction($this->container));
        $this->actionManager->register(new QualificationAction($this->container));
        $this->actionManager->register(new BranchCreationAction($this->container));

        //Helper
        $this->helper->register('quaCICDProjectHelper', '\Kanboard\Plugin\QuaCICD\Helper\QuaCICDProjectHelper');

    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getPluginName()
    {
        return 'QuaCICD';
    }

    public function getPluginDescription()
    {
        return t('This plugin allow you to execute automated deployment in qualification and production directly on your board.');
    }

    public function getPluginAuthor()
    {
        return 'Quamob';
    }

    public function getPluginVersion()
    {
        return '0.0.1';
    }

    public function getPluginHomepage()
    {
        return 'https://gitlab.com/quamob/quaCICD';
    }
}

