<?php

namespace Kanboard\Plugin\QuaCICD\Controller;

use Kanboard\Controller\BaseController;
use Kanboard\Plugin\QuaCICD\Validator\ProjectQuaCICDValidator;
use Kanboard\Plugin\QuaCICD\Helper\HelperQuaCICD;

/**
 * QuaCICDController
 *
 * @package Kanboard\Plugin\QuaCICD\Controller
 * @author  Alexandre Turpin
 */
class QuaCICDController extends BaseController
{
    /**
     * Display a form to create a new quaCICD project
     *
     * @access public
     * @param array $values
     * @param array $errors
     */
    public function create(array $values = array(), array $errors = array())
    {
        isset($values['qualification_colmun_name']) ? : $values['qualification_colmun_name'] = $this->configModel->get('quaCICD_default_qualification_name');
        isset($values['production_colmun_name']) ? : $values['production_colmun_name'] = $this->configModel->get('quaCICD_default_production_name');
        
        $is_private = isset($values['is_private']) && $values['is_private'] == 1;
        $projects_list = array(0 => t('Do not duplicate anything')) + $this->projectUserRoleModel->getActiveProjectsByUser($this->userSession->getId());

        $this->response->html($this->helper->layout->app('QuaCICD:project/createQuaCICD', array(
            'values' => $values,
            'errors' => $errors,
            'is_private' => $is_private,
            'projects_list' => $projects_list,
            'title' => $is_private ? t('New personal QuaCICD project') : t('New QuaCICD project'),
        )));
    }

    /**
     * Validate and save a new quaCICD project
     *
     * @access public
     */
    public function save()
    {
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);

        $values = $this->request->getValues();
        list($valid, $errors) = $projectQuaCICDValidator->validateCreation($values);
        
        if ($valid) {
            $projectQuaCICD_id = $this->createOrDuplicate($values);
            
            if ($projectQuaCICD_id > 0) {

                $project_id = $this->projectQuaCICDModel->getById($projectQuaCICD_id)['project_id'];

                exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh ". HelperQuaCICD::getCreateQuaCICDProjectParameters($this->userSession->getUsername(), $project_id, $values['name']) . " 2>&1", $output, $return);
                if(!$return)
                {                    
                    //Create columns
                    $id_column_qualification = $this->columnModel->create($project_id, $values['qualification_colmun_name'], 0, '');
                    $id_column_production= $this->columnModel->create($project_id, $values['production_colmun_name'], 0, '');

                    //Create actions
                    $this->actionModel->create(array(   
                        "project_id" => $project_id,
                        "event_name" => "task.move.column",
                        "action_name"   => "\Kanboard\Plugin\QuaCICD\Action\ProductionAction",
                        "params" => array('column_id' => $id_column_production),
                    ));
                    $this->actionModel->create(array(   
                        "project_id" => $project_id,
                        "event_name" => "task.move.column",
                        "action_name"   => "\Kanboard\Plugin\QuaCICD\Action\QualificationAction",
                        "params" => array('column_id' => $id_column_qualification),
                    ));
                    $this->actionModel->create(array(   
                        "project_id" => $project_id,
                        "event_name" => "task.move.column",
                        "action_name"   => "\Kanboard\Plugin\QuaCICD\Action\BranchCreationAction",
                        "params" => array('column_id' => ($id_column_production - 3)),
                    ));
                    
                    $this->flash->success(t('Your project has been created successfully.'));
                    return $this->response->redirect($this->helper->url->to('ProjectViewController', 'show', array('project_id' => $project_id)));
                }
                else
                {
                    $this->flash->failure(t('Error while executing Create_quaCICD_project : ') . implode($output));
                    $this->projectModel->remove($project_id);
                    return $this->response->redirect($this->helper->url->to('DashboardController', 'show'));
                }
            }

            $this->flash->failure(t('Unable to create your project.'));
        }

        return $this->create($values, $errors);
    }

    /**
     * Create or duplicate a quaCICD project
     *
     * @access private
     * @param  array  $values
     * @return boolean|integer
     */
    private function createOrDuplicate(array $values)
    {
        if (empty($values['src_project_id'])) {
            return $this->createNewQuaCICDProject($values);
        }

        return $this->duplicateNewQuaCICDProject($values);
    }

    /**
     * Save a new quaCICD project
     *
     * @access private
     * @param  array  $values
     * @return boolean|integer
     */
    private function createNewQuaCICDProject(array $values)
    {
        $project = array(
            'name' => $values['name'],
            'is_private' => $values['is_private'],
            'identifier' => $values['identifier'],
            'per_swimlane_task_limits' => array_key_exists('per_swimlane_task_limits', $values) ? $values['per_swimlane_task_limits'] : 0,
            'task_limit' => $values['task_limit'],
        );

        $project_id = $this->projectModel->create($project, $this->userSession->getId(), true);

        $quaCICD_values['project_id'] = $project_id;
        $quaCICD_values['qualification_colmun_name'] = $values['qualification_colmun_name'];
        $quaCICD_values['production_colmun_name'] = $values['production_colmun_name'];
        $projectQuaCICD_id = $this->projectQuaCICDModel->create($quaCICD_values);

        return $projectQuaCICD_id;
    }

    /**
     * Create from another quaCICD project
     *
     * @access private
     * @param  array  $values
     * @return boolean|integer
     */
    private function duplicateNewQuaCICDProject(array $values)
    {
        $selection = array();

        foreach ($this->projectDuplicationModel->getOptionalSelection() as $item) {
            if (isset($values[$item]) && $values[$item] == 1) {
                $selection[] = $item;
            }
        }
        $project_id = $this->projectDuplicationModel->duplicate(
            $values['src_project_id'],
            $selection,
            $this->userSession->getId(),
            $values['name'],
            $values['is_private'] == 1,
            $values['identifier']
        );

        $quaCICD_values['project_id'] = $project_id;
        $quaCICD_values['qualification_colmun_name'] = $values['qualification_colmun_name'];
        $quaCICD_values['production_colmun_name'] = $values['production_colmun_name'];
        $projectQuaCICD_id = $this->projectQuaCICDModel->create($quaCICD_values);

        return $projectQuaCICD_id;
    }
}