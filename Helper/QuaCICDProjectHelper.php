<?php

namespace Kanboard\Plugin\QuaCICD\Helper;

use Kanboard\Core\Base;

/**
 * QuaCICDProjectHelper
 *
 * @package Kanboard\Plugin\QuaCICD\Helper
 * @author  Alexandre Turpin
 */
class QuaCICDProjectHelper extends Base
{
    /**
     * Test if a given project is a QuaCICD project or not
     * 
     * @access public
     * @param integer $project_id
     * @return boolean
     */
    public function isQuaCICDProject($project_id)
    {
        return $this->projectQuaCICDModel->getByProjectId($project_id) != null; 
    }
}