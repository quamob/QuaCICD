<?php

require_once 'tests/units/Base.php';
require_once 'plugins/QuaCICD/vendor/autoload.php';
require_once 'plugins/QuaCICD/Test/_helper/Fixtures.php';

use Kanboard\Core\Plugin\Loader;
use Kanboard\Plugin\QuaCICD\Plugin;
use Kanboard\Plugin\QuaCICD\Model\ProjectQuaCICDModel;
use Kanboard\Model\ProjectModel;

class ProjectQuaCICDModelTest extends Base
{
    public function setUp()
    {
        parent::setUp();
        $plugin = new Loader($this->container);
        $plugin->scan();
    }

    public function testCreateSuccess()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $projectModel = new ProjectModel($this->container);
        $faker = Faker\Factory::create();

        $project_id = $projectModel->create(array('name' => 'Unit Test Project'));
        
        $values['project_id'] = $project_id;
        $values['qualification_colmun_name'] = Fixtures::randomDataString();
        $values['production_colmun_name'] = Fixtures::randomDataString();

        $this->assertNotFalse($projectQuaCICDModel->create($values));
    }

    public function testGetById()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);

        $project_quaCICD = $this->generateQuaCICDProject(1)[0];

        $this->assertNotEmpty($projectQuaCICDModel->getById($project_quaCICD['id']));
        $this->assertSame($project_quaCICD, $projectQuaCICDModel->getById($project_quaCICD['id']));
        
    }

    public function testGetByIdNotNumber()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $faker = Faker\Factory::create();

        $projects_quaCICD = $this->generateQuaCICDProject(1);

        $project_quaCICD = $projects_quaCICD[0];

        $this->assertNotSame($project_quaCICD, $projectQuaCICDModel->getById(Fixtures::randomDataString()));
        
    }

    public function testGetByIDEmptyString()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);

        $project_quaCICD = $this->generateQuaCICDProject(1)[0];

        $this->assertNotSame($project_quaCICD, $projectQuaCICDModel->getById(''));
        
    }

    public function testGetByProjectId()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);

        $project_quaCICD = $this->generateQuaCICDProject(1)[0];

        $this->assertNotEmpty($projectQuaCICDModel->getByProjectId($project_quaCICD['project_id']));
        $this->assertSame($project_quaCICD, $projectQuaCICDModel->getByProjectId($project_quaCICD['project_id']));
    }

    public function testGetByProjectIdNotNumber()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $faker = Faker\Factory::create();
        $project_quaCICD = $this->generateQuaCICDProject(1)[0];

        $this->assertNotSame($project_quaCICD, $projectQuaCICDModel->getByProjectId(Fixtures::randomDataString()));
    }

    public function testGetByProjectIdEmptyString()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);

        $project_quaCICD = $this->generateQuaCICDProject(1)[0];

        $this->assertNotSame($project_quaCICD, $projectQuaCICDModel->getByProjectId(''));
    }

    public function testGetAll()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        
        $projects_quaCICD = $this->generateQuaCICDProject(3);

        $this->assertSame(3, sizeof($projectQuaCICDModel->getAll()));
        $this->assertSame($projects_quaCICD, $projectQuaCICDModel->getAll());
    }

    public function testGetAllWithEmptyDataBase()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);

        $this->assertSame(0, sizeof($projectQuaCICDModel->getAll()));
    }


    public function testGetAllByIds()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);

        $projects_quaCICD = $this->generateQuaCICDProject(3);

        $this->assertSame(2, sizeof($projectQuaCICDModel->getAllByIds(array($projects_quaCICD[0]['id'], $projects_quaCICD[2]['id']))));
        $this->assertSame([$projects_quaCICD[0], $projects_quaCICD[2]], $projectQuaCICDModel->getAllByIds(array($projects_quaCICD[0]['id'], $projects_quaCICD[2]['id'])));
    }

    public function testGetAllByIdsWithEmptyDataBase()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $faker = Faker\Factory::create();
        $this->assertSame(0, sizeof($projectQuaCICDModel->getAllByIds(array($faker->randomDigitNotNull(), $faker->randomDigitNotNull()))));
    }

    public function testGetAllByIdsWithWrongId()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $faker = Faker\Factory::create();
        $projects_quaCICD = $this->generateQuaCICDProject(3);
        $this->assertSame(1, sizeof($projectQuaCICDModel->getAllByIds(array($projects_quaCICD[0]['id'], $faker->numberBetween($min = 5, $max = 9000)))));
        $this->assertSame([$projects_quaCICD[0]], $projectQuaCICDModel->getAllByIds(array($projects_quaCICD[0]['id'], $faker->numberBetween($min = 5, $max = 9000))));
    }

    public function testGetAllByIdsWithChars()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $faker = Faker\Factory::create();
        $projects_quaCICD = $this->generateQuaCICDProject(3);
        $this->assertSame(0, sizeof($projectQuaCICDModel->getAllByIds(array(Fixtures::randomDataString(), Fixtures::randomDataString()))));
    }

    public function testGetAllByIdsWithNullId()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $faker = Faker\Factory::create();
        $projects_quaCICD = $this->generateQuaCICDProject(3);
        $this->assertSame(0, sizeof($projectQuaCICDModel->getAllByIds(array(0))));
    }

    public function testGetAllIds()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        
        $projects_quaCICD = $this->generateQuaCICDProject(3);

        $this->assertSame(3, sizeof($projectQuaCICDModel->getAllIds()));
        $this->assertSame([$projects_quaCICD[0]['id'], $projects_quaCICD[1]['id'], $projects_quaCICD[2]['id']], $projectQuaCICDModel->getAllIds());
    }

    public function testGetAllIdsWithEmptyDataBase()
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);

        $this->assertSame(0, sizeof($projectQuaCICDModel->getAllIds()));
    }

    private function generateQuaCICDProject($numberOfQuaCICDProjects)
    {
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $projectModel = new ProjectModel($this->container);
        $faker = Faker\Factory::create();

        $projects_quaCICD = array();

        $values['project_id'] = null;
        $values['qualification_colmun_name'] = Fixtures::randomDataString();
        $values['production_colmun_name'] = Fixtures::randomDataString();

        for($i=0; $i<$numberOfQuaCICDProjects;$i++)
        {
            $project_id = (string)$projectModel->create(Fixtures::generateValuesProjectModel());
            $values['project_id'] = $project_id;
            $project_quaCICD_id = (string)$projectQuaCICDModel->create($values);
            $project_quaCICD = ['id' => $project_quaCICD_id] + $values;
            array_push($projects_quaCICD,$project_quaCICD);
        }

        return $projects_quaCICD;
    }
}
