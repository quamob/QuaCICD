<?php

require_once 'tests/units/Base.php';

use Kanboard\Plugin\QuaCICD\Plugin;

class PluginTest extends Base
{
    public function testPlugin()
    {
        $plugin = new Plugin($this->container);
        $this->assertSame(null, $plugin->initialize());
        $this->assertSame(null, $plugin->onStartup());
        $this->assertNotEmpty($plugin->getPluginName());
        $this->assertNotEmpty($plugin->getPluginDescription());
        $this->assertNotEmpty($plugin->getPluginAuthor());
        $this->assertNotEmpty($plugin->getPluginVersion());
        $this->assertNotEmpty($plugin->getPluginHomepage());
    }
    
    public function testDefaultValues()
    {
        $plugin = new Plugin($this->container);
        $plugin->initialize();
        $this->assertSame('Production', $plugin->configModel->getOption('quaCICD_default_production_name'));
        $this->assertSame('Qualification', $plugin->configModel->getOption('quaCICD_default_qualification_name'));
        $this->assertSame('false', $plugin->configModel->getOption('quaCICD_merge_squash_option'));
        $this->assertSame('true', $plugin->configModel->getOption('quaCICD_delete_branch_in_production'));
    }

    public function testAsset() 
    {
        $plugin = new Plugin($this->container);
        $plugin->initialize();
        $this->assertTrue($plugin->hook->exists('template:dashboard:page-header:menu'));
        $this->assertTrue($plugin->hook->exists('template:project-list:menu:after'));
    }

    public function testModel()
    {
        $plugin = new Plugin($this->container);
        $plugin->initialize();
        $this->assertNotEmpty($this->container['projectQuaCICDModel']);
    }

    public function testAction()
    {
        // Before initialization actions are not registered yet so it throws a RuntimeException

        try
        {
            $plugin->actionManager->getAction('\Kanboard\Plugin\QuaCICD\Action\QualificationAction');
            $this->assertTrue(false);
        }
        catch(RuntimeException $e)
        {
            $this->assertTrue(true);
        }

        try
        {
            $plugin->actionManager->getAction('\Kanboard\Plugin\QuaCICD\Action\ProductionAction');
            $this->assertTrue(false);
        }
        catch(RuntimeException $e)
        {
            $this->assertTrue(true);
        }

        $plugin = new Plugin($this->container);
        $plugin->initialize();

        try
        {
            $plugin->actionManager->getAction('\Kanboard\Plugin\QuaCICD\Action\QualificationAction');
            $this->assertTrue(true);
        }
        catch(RuntimeException $e)
        {
            $this->assertFalse(true);
        }

        try
        {
            $plugin->actionManager->getAction('\Kanboard\Plugin\QuaCICD\Action\ProductionAction');
            $this->assertTrue(true);
        }
        catch(RuntimeException $e)
        {
            $this->assertFalse(true);
        }
    }

    public function testDefaultValuesAlreadyChanged()
    {
        $plugin = new Plugin($this->container);
        $plugin->initialize();

        $values['quaCICD_default_production_name'] = 'ProductionChanged';
        $values['quaCICD_default_qualification_name'] = 'QualificationChanged';
        $values['quaCICD_merge_squash_option'] = 'false';

        $plugin->configModel->save($values);

        $plugin->initialize();

        $this->assertSame('ProductionChanged', $plugin->configModel->getOption('quaCICD_default_production_name'));
        $this->assertSame('QualificationChanged', $plugin->configModel->getOption('quaCICD_default_qualification_name'));
        $this->assertSame('false', $plugin->configModel->getOption('quaCICD_merge_squash_option'));
        $this->assertSame('true', $plugin->configModel->getOption('quaCICD_delete_branch_in_production'));
    }
}
