<?php

require_once 'tests/units/Base.php';
require_once 'plugins/QuaCICD/vendor/autoload.php';
require_once 'plugins/QuaCICD/Test/_helper/Fixtures.php';

use Kanboard\Plugin\QuaCICD\Helper\HelperQuaCICD;
use Kanboard\Core\Plugin\Loader;

class HelperQuaCICDTest extends Base
{
    public function setUp()
    {
        parent::setUp();
        Fixtures::deleteTestEnvironments();
        $plugin = new Loader($this->container);
        $plugin->scan();
    }

    public function tearDown()
    {
        parent::tearDown();

        Fixtures::deleteTestEnvironments();
    }
    public function testGetPathGitRepository(){
        $username = Fixtures::randomDataString();
        $projectName = Fixtures::randomDataString();
        exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh ". HelperQuaCICD::getCreateQuaCICDProjectParameters($username, 1, $projectName) .  " 2>&1", $output, $return);
        $this->assertTrue(is_dir(HelperQuaCICD::getPathGitRepository($username, 1, $projectName)));
    }

    public function testGetPathEnvironment(){
        $username = Fixtures::randomDataString();
        $projectName = Fixtures::randomDataString();
        exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh ". HelperQuaCICD::getCreateQuaCICDProjectParameters($username, 1, $projectName) .  " 2>&1", $output, $return);
        $this->assertTrue(is_dir(HelperQuaCICD::getPathEnvironment($username, 1, $projectName)));
    }

    public function testGetPathScripts(){
        $username = Fixtures::randomDataString();
        $projectName = Fixtures::randomDataString();
        exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh ". HelperQuaCICD::getCreateQuaCICDProjectParameters($username, 1, $projectName) .  " 2>&1", $output, $return);
        $this->assertTrue(is_dir(HelperQuaCICD::getPathScripts($username, 1, $projectName)));
    }

    public function testProjectNameWithSlash()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify("[a-zA-Z/0-9]{300,315}");

        $this->assertSame(str_replace('/', '_', $projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithBackSlash()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify("[a-zA-Z\\0-9]{300,315}");

        $this->assertSame(str_replace('\\', '_', $projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithParentheses()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify("[a-zA-Z()0-9]{300,315}");

        $same = str_replace(')', '_', $projectName);
        $same = str_replace('(', '_', $same);

        $this->assertSame($same, $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithArrows()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify("[a-zA-Z<>0-9]{300,315}");

        $same = str_replace('<', '', $projectName);
        $same = str_replace('>', '', $same);

        $this->assertSame($same, $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithSpace()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify("[a-zA-Z 0-9]{300,315}");
        $this->assertSame(str_replace(' ', '_', $projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithDollar()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify("[a-zA-Z$0-9]{300,315}");
        $this->assertSame(str_replace('$', '', $projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithSemiColon()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify("[a-zA-Z;0-9]{300,315}");
        $this->assertSame(str_replace(';', '', $projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithDot()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = str_replace('a', '.', $faker->regexify("[a-zA-Z0-9]{300,315}"));
        $this->assertSame(str_replace('.', '_', $projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithCaret()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify("[a-zA-Z^0-9]{300,315}");
        $this->assertSame(str_replace('^', '', $projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithColon()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify("[a-zA-Z:0-9]{300,315}");
        $this->assertSame(str_replace(':', '_', $projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithExclamationMark()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify("[a-zA-Z!0-9]{300,315}");
        $this->assertSame(str_replace('!', '', $projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithQuotation()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify('[a-zA-Z"0-9]{300,315}');
        $this->assertSame(str_replace('"', '', $projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithApostrophe()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $projectName = $faker->regexify("[a-zA-Z'0-9]{300,315}");
        $this->assertSame(str_replace("'", "", $projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testProjectNameWithAccent()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        
        $projectName = $faker->regexify("[a-zA-Z0-9éàçâ]{300,315}");
        $this->assertSame(HelperQuaCICD::remove_accents($projectName), $helperQuaCICD->formatProjectName($projectName));
    }

    public function testTaskNameWithSpace()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("[a-zA-Z 0-9]{300,315}");
        $this->assertSame(str_replace(' ', '_', $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithDot()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = str_replace('a', '.', $faker->regexify("[a-zA-Z0-9]{300,315}"));
        $this->assertSame(str_replace('.', '_', $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithLeftBrackets()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("[a-zA-Z(0-9]{300,315}");
        $this->assertSame(str_replace('(', '', $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithRightBrackets()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("[a-zA-Z)0-9]{300,315}");
        $this->assertSame(str_replace(')', '', $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithSemiColon()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("[a-zA-Z;0-9]{300,315}");
        $this->assertSame(str_replace(';', '', $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithBackSlash()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("[a-zA-Z\\0-9]{300,315}");
        $this->assertSame(str_replace('\\', '_', $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithTilde()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("[a-zA-Z~0-9]{300,315}");
        $this->assertSame(str_replace('~', '', $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithCaret()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("[a-zA-Z^0-9]{300,315}");
        $this->assertSame(str_replace('^', '', $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithColon()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("[a-zA-Z:0-9]{300,315}");
        $this->assertSame(str_replace(':', '_', $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithSlashAtEnd()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("[a-zA-Z0-9]{300,315}") . "/";
        $this->assertSame(substr($taskName, 0, -1), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithQuotation()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify('[a-zA-Z"0-9]{300,315}');
        $this->assertSame(str_replace('"', '', $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithApostrophe()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("[a-zA-Z'0-9]{300,315}");
        $this->assertSame(str_replace("'", "", $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithDash()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("-[a-zA-Z0-9]{300,315}");
        $this->assertSame(str_replace("-", "_", $taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testTaskNameWithAccent()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $taskName = $faker->regexify("[a-zA-Z0-9éàçâ]{300,315}");
        $this->assertSame(HelperQuaCICD::remove_accents($taskName), $helperQuaCICD->formatTaskTitle($taskName));
    }

    public function testUsernameWithAccent()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $username = $faker->regexify("[a-zA-Z0-9éàçâ]{300,315}");
        $this->assertSame(HelperQuaCICD::remove_accents($username), $helperQuaCICD->formatUsername($username));
    }

    public function testUsernameWithApostrophe()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $username = $faker->regexify("[a-zA-Z'0-9]{300,315}");
        $this->assertSame(str_replace("'", "", $username), $helperQuaCICD->formatUsername($username));
    }

    public function testUsernameWithDot()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $username = str_replace('a', '.', $faker->regexify("[a-zA-Z0-9]{300,315}"));
        $this->assertSame(str_replace(".", "_", $username), $helperQuaCICD->formatUsername($username));
    }
    
    public function testUsernameWithBackSlash()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $username = $faker->regexify("[a-zA-Z\\0-9]{300,315}");
        $this->assertSame(str_replace('\\', '_', $username), $helperQuaCICD->formatUsername($username));
    }

    public function testUsernameWithTilde()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $username = $faker->regexify("[a-zA-Z~0-9]{300,315}");
        $this->assertSame(str_replace('~', '', $username), $helperQuaCICD->formatUsername($username));
    }

    public function testUsernameWithCaret()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $username = $faker->regexify("[a-zA-Z^0-9]{300,315}");
        $this->assertSame(str_replace('^', '', $username), $helperQuaCICD->formatUsername($username));
    }

    public function testUsernameWithColon()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $username = $faker->regexify("[a-zA-Z:0-9]{300,315}");
        $this->assertSame(str_replace(':', '_', $username), $helperQuaCICD->formatUsername($username));
    }

    public function testUsernameWithQuotation()
    {
        $faker = Faker\Factory::create();
        $helperQuaCICD = new HelperQuaCICD($this->container);
        $username = $faker->regexify('[a-zA-Z"0-9]{300,315}');
        $this->assertSame(str_replace('"', '', $username), $helperQuaCICD->formatUsername($username));
    }
}
