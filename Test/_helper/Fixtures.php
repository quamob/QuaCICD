<?php

require_once 'plugins/QuaCICD/vendor/autoload.php';

class Fixtures {

    public static function randomDataLongId()
    {
        $faker = Faker\Factory::create();

        return $faker->regexify('[1-9]{2,255}');
    }

    public static function randomDataId()
    {
        $faker = Faker\Factory::create();

        return $faker->regexify('[0-9]{1,5}');
    }

    public static function randomDataString()
    {
        $faker = Faker\Factory::create();

        return $faker->regexify("[a-zA-Z._%+-@!?]{1,25}");
    }

    public static function randomDataLongString()
    {
        $faker = Faker\Factory::create();

        return $faker->regexify("[a-zA-Z0-9._%+-@!?]{300,315}");
    }

    public static function generateValuesActions()
    {
        $values['project_id'] = 1;
        $values['qualification_colmun_name'] = "Qualification";
        $values['production_colmun_name'] = "Production";

        return $values;
    }

    public static function generateValuesValidator($fieldNotWanted = array())
    {
        $faker = Faker\Factory::create();

        $fieldNames = array('name', 'qualification_colmun_name', 'production_colmun_name');
        
        foreach($fieldNames as $field)
        {
            if(!in_array($field, $fieldNotWanted)){
                $values[$field] = Fixtures::randomDataString();
            }
        }

        return $values;
    }

    public static function generateValuesProjectModel($fieldNotWanted = array())
    {
        $faker = Faker\Factory::create();

        $fieldNames = array('name');
        
        foreach($fieldNames as $field)
        {
            if(!in_array($field, $fieldNotWanted)){
                $values[$field] = Fixtures::randomDataString();
            }
        }

        return $values;
    }

    public function deleteTestEnvironments()
    {
        if(file_exists('/var/www/html/QuaCICD_Test/')){
            Fixtures::delete_files('/var/www/html/QuaCICD_Test/');
        }

        if(file_exists('/srv/QuaCICD_Test/')){
            Fixtures::delete_files('/srv/QuaCICD_Test/');
        }
    }

    /* 
    * php delete function that deals with directories recursively
    */
    public function delete_files($dir) {
        if (is_dir($dir)) { 
            $objects = scandir($dir);
            foreach ($objects as $object) { 
                if ($object != "." && $object != "..") { 
                    if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
                        Fixtures::delete_files($dir. DIRECTORY_SEPARATOR .$object);
                    else
                        unlink($dir. DIRECTORY_SEPARATOR .$object); 
                } 
            }
            rmdir($dir); 
        }
        elseif(is_file($dir)) {
            unlink( $dir );  
        }
    }

}