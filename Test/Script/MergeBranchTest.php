<?php

class MergeBranchTest extends Base
{
    public function setUp()
    {
        parent::setUp();
        Fixtures::deleteTestEnvironments();
    }

    public function tearDown()
    {
        parent::tearDown();

        Fixtures::deleteTestEnvironments();
    }

    public static function tearDownAfterClass()
    {
        parent::tearDownAfterClass();

        Fixtures::deleteTestEnvironments();
    }
    public function testMerge(){

        $git_repository = '/srv/' . QUACICD_SUBDIR_ENV . '/Git/testMerge.git';
        $environment_path = '/srv/' . QUACICD_SUBDIR_ENV . '/env/';
        $branch_to_merge = 'Task/1-test_merge';
        $squash_option = 'true';
        $branch_to_merge_into = 'master';

        $merge_branch_script_path = '/srv/' . QUACICD_SUBDIR_ENV . '/script';
        
        mkdir($merge_branch_script_path, 0777, true);
        copy('plugins/QuaCICD/Script/Merge_Branch.sh', $merge_branch_script_path . '/Merge_Branch.sh');
        chmod($merge_branch_script_path . '/Merge_Branch.sh', fileperms('plugins/QuaCICD/Script/Merge_Branch.sh'));

        mkdir($environment_path, 0777, true);
        mkdir($git_repository, 0777, true);
        chdir($git_repository);
        exec('git init --bare');
        exec('git config --global user.email "dev@localhost"' .  " 2>&1", $output, $return);
        exec('git config --global user.name "Dev Folder"' .  " 2>&1", $output, $return);

        chdir($environment_path);
        exec('git clone ' . $git_repository .  " 2>&1", $output, $return);
        chdir('testMerge');
        file_put_contents('Readme.md', 'testMerge');
        exec('git add .' .  " 2>&1", $output, $return);
        exec('git commit -m "First commit"' .  " 2>&1", $output, $return);
        exec('git push' .  " 2>&1", $output, $return);
        exec('git checkout -b ' . $branch_to_merge .  " 2>&1", $output, $return);
        file_put_contents('Readme.md', 'testMerge in ' . $branch_to_merge);
        exec('git add .' .  " 2>&1", $output, $return);
        exec('git commit -m "testMerge"' .  " 2>&1", $output, $return);
        exec('git push --set-upstream origin ' . $branch_to_merge .  " 2>&1", $output, $return);

        exec($merge_branch_script_path . "/Merge_Branch.sh " . $git_repository . " " . $environment_path . " " . $branch_to_merge . " " . $squash_option . " " . $branch_to_merge_into .  " 2>&1", $outputScript, $return);
        $outputScript = array_slice($outputScript, -6 , -2);
        $outputMergeMaster = array_splice($outputScript, 2, 1);
        $this->assertSame(1, preg_match('/\[master [a-z0-9]{6,}\] Task\/1-test_merge/', $outputMergeMaster[0]));
        $this->assertSame(array(' Readme.md | 2 +-', ' 1 file changed, 1 insertion(+), 1 deletion(-)', ' 1 file changed, 1 insertion(+), 1 deletion(-)'), $outputScript);

    }

    public function testMergeNoSquash(){

        $git_repository = '/srv/' . QUACICD_SUBDIR_ENV . '/Git/testMerge.git';
        $environment_path = '/srv/' . QUACICD_SUBDIR_ENV . '/env/';
        $branch_to_merge = 'Task/1-test_merge';
        $squash_option = 'false';
        $branch_to_merge_into = 'master';

        $merge_branch_script_path = '/srv/' . QUACICD_SUBDIR_ENV . '/script';
        
        mkdir($merge_branch_script_path, 0777, true);
        copy('plugins/QuaCICD/Script/Merge_Branch.sh', $merge_branch_script_path . '/Merge_Branch.sh');
        chmod($merge_branch_script_path . '/Merge_Branch.sh', fileperms('plugins/QuaCICD/Script/Merge_Branch.sh'));

        mkdir($environment_path, 0777, true);
        mkdir($git_repository, 0777, true);
        chdir($git_repository);
        exec('git init --bare' .  " 2>&1", $output, $return);

        chdir($environment_path);
        exec('git clone ' . $git_repository .  " 2>&1", $output, $return);
        chdir('testMerge');
        file_put_contents('Readme.md', 'testMerge');
        exec('git add .' .  " 2>&1", $output, $return);
        exec('git commit -m "First commit"' .  " 2>&1", $output, $return);
        exec('git push' .  " 2>&1", $output, $return);
        exec('git checkout -b ' . $branch_to_merge .  " 2>&1", $output, $return);
        file_put_contents('Readme.md', 'testMerge in ' . $branch_to_merge);
        exec('git add .' .  " 2>&1", $output, $return);
        exec('git commit -m "testMerge"' .  " 2>&1", $output, $return);
        exec('git push --set-upstream origin ' . $branch_to_merge .  " 2>&1", $output, $return);

        exec($merge_branch_script_path . "/Merge_Branch.sh " . $git_repository . " " . $environment_path . " " . $branch_to_merge . " " . $squash_option . " " . $branch_to_merge_into .  " 2>&1", $outputScript, $return);

        $outputScript = array_slice($outputScript, -4, -2);
        $this->assertSame(array(' Readme.md | 2 +-', ' 1 file changed, 1 insertion(+), 1 deletion(-)'), $outputScript);

    }

    public function testMergeConflict(){

        $git_repository = '/srv/' . QUACICD_SUBDIR_ENV . '/Git/testMerge.git';
        $environment_path = '/srv/' . QUACICD_SUBDIR_ENV . '/env/';
        $branch_to_merge = 'Task/1-test_merge';
        $squash_option = 'true';
        $branch_to_merge_into = 'master';

        $merge_branch_script_path = '/srv/' . QUACICD_SUBDIR_ENV . '/script';
        
        mkdir($merge_branch_script_path, 0777, true);
        copy('plugins/QuaCICD/Script/Merge_Branch.sh', $merge_branch_script_path . '/Merge_Branch.sh');
        chmod($merge_branch_script_path . '/Merge_Branch.sh', fileperms('plugins/QuaCICD/Script/Merge_Branch.sh'));

        mkdir($environment_path, 0777, true);
        mkdir($git_repository, 0777, true);
        chdir($git_repository);
        exec('git init --bare' .  " 2>&1", $output, $return);

        chdir($environment_path);
        exec('git clone ' . $git_repository .  " 2>&1", $output, $return);
        chdir('testMerge');
        file_put_contents('Readme.md', 'testMerge');
        exec('git add .' .  " 2>&1", $output, $return);
        exec('git commit -m "First commit"' .  " 2>&1", $output, $return);
        exec('git push' .  " 2>&1", $output, $return);
        exec('git checkout -b ' . $branch_to_merge .  " 2>&1", $output, $return);
        file_put_contents('Readme.md', 'testMerge in ' . $branch_to_merge);
        exec('git add .' .  " 2>&1", $output, $return);
        exec('git commit -m "testMerge"' .  " 2>&1", $output, $return);
        exec('git push --set-upstream origin ' . $branch_to_merge .  " 2>&1", $output, $return);

        exec('git checkout master' .  " 2>&1", $output, $return);
        file_put_contents('Readme.md', 'testMergeConflict');
        exec('git add .' .  " 2>&1", $output, $return);
        exec('git commit -m "First commit"' .  " 2>&1", $output, $return);
        exec('git push' .  " 2>&1", $output, $return);

        exec($merge_branch_script_path . "/Merge_Branch.sh " . $git_repository . " " . $environment_path . " " . $branch_to_merge . " " . $squash_option . " " . $branch_to_merge_into .  " 2>&1", $outputScript, $return);
        $outputScript = array_slice($outputScript, -2);
        $this->assertSame(array('There is a merge conflict.  Check comments to resolve it.', 'Readme.md Readme.md'), $outputScript);

    }

    public function testMergeConflictNoSquash(){

        $git_repository = '/srv/' . QUACICD_SUBDIR_ENV . '/Git/testMerge.git';
        $environment_path = '/srv/' . QUACICD_SUBDIR_ENV . '/env/';
        $branch_to_merge = 'Task/1-test_merge';
        $squash_option = 'false';
        $branch_to_merge_into = 'master';

        $merge_branch_script_path = '/srv/' . QUACICD_SUBDIR_ENV . '/script';
        
        mkdir($merge_branch_script_path, 0777, true);
        copy('plugins/QuaCICD/Script/Merge_Branch.sh', $merge_branch_script_path . '/Merge_Branch.sh');
        chmod($merge_branch_script_path . '/Merge_Branch.sh', fileperms('plugins/QuaCICD/Script/Merge_Branch.sh'));

        mkdir($environment_path, 0777, true);
        mkdir($git_repository, 0777, true);
        chdir($git_repository);
        exec('git init --bare' .  " 2>&1", $output, $return);

        chdir($environment_path);
        exec('git clone ' . $git_repository .  " 2>&1", $output, $return);
        chdir('testMerge');
        file_put_contents('Readme.md', 'testMerge');
        exec('git add .' .  " 2>&1", $output, $return);
        exec('git commit -m "First commit"' .  " 2>&1", $output, $return);
        exec('git push' .  " 2>&1", $output, $return);
        exec('git checkout -b ' . $branch_to_merge .  " 2>&1", $output, $return);
        file_put_contents('Readme.md', 'testMerge in ' . $branch_to_merge);
        exec('git add .' .  " 2>&1", $output, $return);
        exec('git commit -m "testMerge"' .  " 2>&1", $output, $return);
        exec('git push --set-upstream origin ' . $branch_to_merge .  " 2>&1", $output, $return);

        exec('git checkout master' .  " 2>&1", $output, $return);
        file_put_contents('Readme.md', 'testMergeConflict');
        exec('git add .' .  " 2>&1", $output, $return);
        exec('git commit -m "First commit"' .  " 2>&1", $output, $return);
        exec('git push' .  " 2>&1", $output, $return);

        exec($merge_branch_script_path . "/Merge_Branch.sh " . $git_repository . " " . $environment_path . " " . $branch_to_merge . " " . $squash_option . " " . $branch_to_merge_into .  " 2>&1", $outputScript, $return);
        
        $outputScript = array_slice($outputScript, -2);
        $this->assertSame(array('There is a merge conflict.  Check comments to resolve it.', 'Readme.md Readme.md'), $outputScript);

    }
}