<?php

require_once 'tests/units/Base.php';
require_once 'plugins/QuaCICD/vendor/autoload.php';
require_once 'plugins/QuaCICD/Test/_helper/Fixtures.php';

use Kanboard\Plugin\QuaCICD\Plugin;
use Kanboard\Plugin\QuaCICD\Controller\QuaCICDController;
use Kanboard\Plugin\QuaCICD\Model\ProjectQuaCICDModel;
use Kanboard\Core\Plugin\Loader;
use Kanboard\Core\Security\Token;
use Kanboard\Model\UserModel;
use Kanboard\Model\ColumnModel;
use Kanboard\Model\ActionModel;
use Kanboard\Model\ActionParameterModel;
use Kanboard\Core\Http\RememberMeCookie;
use Kanboard\Core\Session\SessionHandler;
use Symfony\Component\Process\Process;

class ProjectQuaCICDControllerTest extends Base
{
    private static $process;

    public function setUp()
    {
        parent::setUp();
        Fixtures::deleteTestEnvironments();
        $plugin = new Loader($this->container);
        $plugin->scan();

        self::$process = new Process(array('php', '-S', 'localhost:8080', '-t', '.'));
        self::$process->start(null, ['QUACICD_SUBDIR_ENV' => QUACICD_SUBDIR_ENV, 'DB_FILENAME' => DB_FILENAME]);

        usleep(100000);
    }

    public function tearDown()
    {
        parent::tearDown();

        Fixtures::deleteTestEnvironments();
    }

    public function testSaveSuccess()
    {
        $client = new GuzzleHttp\Client(['cookies' => true]);

        $tokenGenerator = new Token($this->container);
        $cookieGenerator = new RememberMeCookie($this->container);
        $sessionHandler = new SessionHandler($this->container['db']);

        $jar = GuzzleHttp\Cookie\CookieJar::fromArray(
            [
                'KB_SID' => '1234'
            ],
            'localhost'
        );

        $userModel = new UserModel($this->container);
        $userModel->create(array('username' => 'userQuaCICD1', 'password' => '123456', 'email' => 'userQuaCICD1@localhost'));

        $token = $tokenGenerator->getCSRFToken();
        $cookieEncode = $cookieGenerator->encode('csrf', serialize(array($token => true)));
        $sessionHandler->write('1234',$cookieEncode);

        $test = $userModel->getAll();

        $responseLogin = $client->request('POST', 'http://localhost:8080/?controller=AuthController&action=check', [
            'form_params' => [
                'csrf_token' => $token,
                'username' => 'userQuaCICD1',
                'password' => '123456',
                'remember_me' => '1',
            ],
            'cookies' => $jar,
        ]);

        $token = $tokenGenerator->getCSRFToken();
        $cookieEncode = $cookieGenerator->encode('csrf', serialize(array($token => true)));
        $sessionHandler->write('1234',$cookieEncode);

        $response = $client->request('POST', 'http://localhost:8080/?controller=QuaCICDController&action=save&plugin=QuaCICD', [
            'form_params' => [
                'csrf_token' => $token,
                'is_private' => '',
                'name' => 'My Project',
                'identifier' => '',
                'task_limit' => '',
                'src_project_id' => '0',
                'projectPermissionModel' => '1',
                'projectRoleModel' => '1',
                'categoryModel' => '1',
                'tagDuplicationModel' => '1',
                'actionModel' => '1',
                'customFilterModel' => '1',
                'qualification_colmun_name' => 'Qualification',
                'production_colmun_name' => 'Production',
                'programming_language' => '0'
            ],
            'cookies' => $jar,
        ]);

        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $this->assertSame(1,count($projectQuaCICDModel->getAll()));
    }

    public function testActionsCreateSuccess()
    {
        $client = new GuzzleHttp\Client(['cookies' => true]);

        $tokenGenerator = new Token($this->container);
        $cookieGenerator = new RememberMeCookie($this->container);
        $sessionHandler = new SessionHandler($this->container['db']);

        $jar = GuzzleHttp\Cookie\CookieJar::fromArray(
            [
                'KB_SID' => '1234'
            ],
            'localhost'
        );

        $userModel = new UserModel($this->container);
        $userModel->create(array('username' => 'userQuaCICD1', 'password' => '123456', 'email' => 'userQuaCICD1@localhost'));

        $token = $tokenGenerator->getCSRFToken();
        $cookieEncode = $cookieGenerator->encode('csrf', serialize(array($token => true)));
        $sessionHandler->write('1234',$cookieEncode);

        $test = $userModel->getAll();

        $responseLogin = $client->request('POST', 'http://localhost:8080/?controller=AuthController&action=check', [
            'form_params' => [
                'csrf_token' => $token,
                'username' => 'userQuaCICD1',
                'password' => '123456',
                'remember_me' => '1',
            ],
            'cookies' => $jar,
        ]);

        $token = $tokenGenerator->getCSRFToken();
        $cookieEncode = $cookieGenerator->encode('csrf', serialize(array($token => true)));
        $sessionHandler->write('1234',$cookieEncode);

        $response = $client->request('POST', 'http://localhost:8080/?controller=QuaCICDController&action=save&plugin=QuaCICD', [
            'form_params' => [
                'csrf_token' => $token,
                'is_private' => '',
                'name' => 'My Project',
                'identifier' => '',
                'task_limit' => '',
                'src_project_id' => '0',
                'projectPermissionModel' => '1',
                'projectRoleModel' => '1',
                'categoryModel' => '1',
                'tagDuplicationModel' => '1',
                'actionModel' => '1',
                'customFilterModel' => '1',
                'qualification_colmun_name' => 'Qualification',
                'production_colmun_name' => 'Production',
                'programming_language' => '0'
            ],
            'cookies' => $jar,
        ]);

        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $this->assertSame(1,count($projectQuaCICDModel->getAll()));

        $columnModel = new ColumnModel($this->container);
        $allProjectColumn = $columnModel->getAll(1);
        $allIdColumns  = array();
        foreach($allProjectColumn as $column){
            array_push($allIdColumns, $column['id']);
        }

        $actionModel = new ActionModel($this->container);
        $allActions = $actionModel->getAllByProject(1);
        $allIdActions = array();
        foreach($allActions as $actions){
            array_push($allIdActions, $actions['id']);
        }

        $actionParameterModel = new ActionParameterModel($this->container);
        $allParamsActions = $actionParameterModel->getAllByActions($allIdActions);

        foreach($allParamsActions as $paramsActions){
            $this->assertContains($paramsActions['column_id'], $allIdColumns);
        }
    }

    public function testActionsCreateSuccessWithTwoProjects()
    {
        $client = new GuzzleHttp\Client(['cookies' => true]);

        $tokenGenerator = new Token($this->container);
        $cookieGenerator = new RememberMeCookie($this->container);
        $sessionHandler = new SessionHandler($this->container['db']);

        $jar = GuzzleHttp\Cookie\CookieJar::fromArray(
            [
                'KB_SID' => '1234'
            ],
            'localhost'
        );

        $userModel = new UserModel($this->container);
        $userModel->create(array('username' => 'userQuaCICD1', 'password' => '123456', 'email' => 'userQuaCICD1@localhost'));

        $token = $tokenGenerator->getCSRFToken();
        $cookieEncode = $cookieGenerator->encode('csrf', serialize(array($token => true)));
        $sessionHandler->write('1234',$cookieEncode);

        $test = $userModel->getAll();

        $responseLogin = $client->request('POST', 'http://localhost:8080/?controller=AuthController&action=check', [
            'form_params' => [
                'csrf_token' => $token,
                'username' => 'userQuaCICD1',
                'password' => '123456',
                'remember_me' => '1',
            ],
            'cookies' => $jar,
        ]);

        $token = $tokenGenerator->getCSRFToken();
        $cookieEncode = $cookieGenerator->encode('csrf', serialize(array($token => true)));
        $sessionHandler->write('1234',$cookieEncode);

        $response = $client->request('POST', 'http://localhost:8080/?controller=QuaCICDController&action=save&plugin=QuaCICD', [
            'form_params' => [
                'csrf_token' => $token,
                'is_private' => '',
                'name' => 'My Project',
                'identifier' => '',
                'task_limit' => '',
                'src_project_id' => '0',
                'projectPermissionModel' => '1',
                'projectRoleModel' => '1',
                'categoryModel' => '1',
                'tagDuplicationModel' => '1',
                'actionModel' => '1',
                'customFilterModel' => '1',
                'qualification_colmun_name' => 'Qualification',
                'production_colmun_name' => 'Production',
                'programming_language' => '0'
            ],
            'cookies' => $jar,
        ]);

        $token = $tokenGenerator->getCSRFToken();
        $cookieEncode = $cookieGenerator->encode('csrf', serialize(array($token => true)));
        $sessionHandler->write('1234',$cookieEncode);

        $response = $client->request('POST', 'http://localhost:8080/?controller=QuaCICDController&action=save&plugin=QuaCICD', [
            'form_params' => [
                'csrf_token' => $token,
                'is_private' => '',
                'name' => 'My Project 2',
                'identifier' => '',
                'task_limit' => '',
                'src_project_id' => '0',
                'projectPermissionModel' => '1',
                'projectRoleModel' => '1',
                'categoryModel' => '1',
                'tagDuplicationModel' => '1',
                'actionModel' => '1',
                'customFilterModel' => '1',
                'qualification_colmun_name' => 'Qualification',
                'production_colmun_name' => 'Production',
                'programming_language' => '0'
            ],
            'cookies' => $jar,
        ]);

        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $this->assertSame(2,count($projectQuaCICDModel->getAll()));

        $columnModel = new ColumnModel($this->container);
        $allProjectColumn = $columnModel->getAll(2);
        $allIdColumns  = array();
        foreach($allProjectColumn as $column){
            array_push($allIdColumns, $column['id']);
        }

        $actionModel = new ActionModel($this->container);
        $allActions = $actionModel->getAllByProject(2);
        $allIdActions = array();
        foreach($allActions as $actions){
            array_push($allIdActions, $actions['id']);
        }

        $actionParameterModel = new ActionParameterModel($this->container);
        $allParamsActions = $actionParameterModel->getAllByActions($allIdActions);

        foreach($allParamsActions as $paramsActions){
            $this->assertContains($paramsActions['column_id'], $allIdColumns);
        }
    }

}
