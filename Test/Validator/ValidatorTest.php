<?php

require_once 'tests/units/Base.php';
require_once 'plugins/QuaCICD/vendor/autoload.php';
require_once 'plugins/QuaCICD/Test/_helper/Fixtures.php';

use Kanboard\Plugin\QuaCICD\Plugin;
use Kanboard\Plugin\QuaCICD\Validator\ProjectQuaCICDValidator;

class ValidatorTest extends Base
{
    public function testValidateCreation()
    {
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);

        $values = Fixtures::generateValuesValidator();

        $return = $projectQuaCICDValidator->validateCreation($values);

        $this->assertTrue($return[0]);
        $this->assertEmpty($return[1]);
    }

    public function testValidateCreationWithoutName()
    {
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);
        
        $values = Fixtures::generateValuesValidator(array('name'));

        $return = $projectQuaCICDValidator->validateCreation($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['name'][0], t('The project name is required'));
    }

    public function testValidateCreationWithoutQualificationName()
    {
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);
        
        $values = Fixtures::generateValuesValidator(array('qualification_colmun_name'));

        $return = $projectQuaCICDValidator->validateCreation($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['qualification_colmun_name'][0], t('The qualification column name is required'));
    }

    public function testValidateCreationWithoutProductionName()
    {
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);
        
        $values = Fixtures::generateValuesValidator(array('production_colmun_name'));

        $return = $projectQuaCICDValidator->validateCreation($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['production_colmun_name'][0], t('The production column name is required'));
    }

    public function testValidateCreationWithoutValues()
    {
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);

        $values = array();


        $return = $projectQuaCICDValidator->validateCreation($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['name'][0], t('The project name is required'));
        $this->assertSame($return[1]['qualification_colmun_name'][0], t('The qualification column name is required'));
        $this->assertSame($return[1]['production_colmun_name'][0], t('The production column name is required'));
    }

    public function testValidateModification()
    {
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);

        $values = Fixtures::generateValuesValidator();

        $return = $projectQuaCICDValidator->validateModification($values);

        $this->assertTrue($return[0]);
    }

    public function testValidateModificationWithoutName()
    {
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);

        $values = Fixtures::generateValuesValidator(array('name'));

        $return = $projectQuaCICDValidator->validateModification($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['name'][0], t('The project name is required'));
    }

    public function testValidateModificationWithoutQualificationName()
    {
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);
        
        $values = Fixtures::generateValuesValidator(array('qualification_colmun_name'));

        $return = $projectQuaCICDValidator->validateModification($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['qualification_colmun_name'][0], t('The qualification column name is required'));
    }

    public function testValidateModificationWithoutProductionName()
    {
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);
        
        $values = Fixtures::generateValuesValidator(array('production_colmun_name'));

        $return = $projectQuaCICDValidator->validateModification($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['production_colmun_name'][0], t('The production column name is required'));
    }

    public function testValidateModificationWithoutValues()
    {
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);

        $values = array();


        $return = $projectQuaCICDValidator->validateModification($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['name'][0], t('The project name is required'));
        $this->assertSame($return[1]['qualification_colmun_name'][0], t('The qualification column name is required'));
        $this->assertSame($return[1]['production_colmun_name'][0], t('The production column name is required'));
    }

    public function testValidateCreationWithLongProductionName(){
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);

        $values = Fixtures::generateValuesValidator();

        $faker = Faker\Factory::create();
        $values['production_colmun_name'] = Fixtures::randomDataLongString();
        
        $return = $projectQuaCICDValidator->validateCreation($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['production_colmun_name'][0], t('The maximum length is 255 characters'));
    }

    public function testValidateModificationWithLongProductionName(){
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);

        $values = Fixtures::generateValuesValidator();

        $faker = Faker\Factory::create();
        $values['production_colmun_name'] = Fixtures::randomDataLongString();
        
        $return = $projectQuaCICDValidator->validateModification($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['production_colmun_name'][0], t('The maximum length is 255 characters'));
    }

    public function testValidateCreationWithLongQualificationName(){
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);

        $values = Fixtures::generateValuesValidator();

        $faker = Faker\Factory::create();
        $values['qualification_colmun_name'] = Fixtures::randomDataLongString();
        
        $return = $projectQuaCICDValidator->validateCreation($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['qualification_colmun_name'][0], t('The maximum length is 255 characters'));
    }

    public function testValidateModificationWithLongQualificationName(){
        $projectQuaCICDValidator = new ProjectQuaCICDValidator($this->container);

        $values = Fixtures::generateValuesValidator();

        $faker = Faker\Factory::create();
        $values['production_colmun_name'] = Fixtures::randomDataLongString();
        
        $return = $projectQuaCICDValidator->validateModification($values);

        $this->assertFalse($return[0]);
        $this->assertSame($return[1]['production_colmun_name'][0], t('The maximum length is 255 characters'));
    }
}
