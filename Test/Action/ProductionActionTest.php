<?php

require_once 'tests/units/Base.php';

use Kanboard\Plugin\QuaCICD\Plugin;
use Kanboard\Core\Plugin\Loader;
use Kanboard\Plugin\QuaCICD\Action\ProductionAction;
use Kanboard\Plugin\QuaCICD\Action\QualificationAction;
use Kanboard\Plugin\QuaCICD\Action\BranchCreationAction;
use Kanboard\Plugin\QuaCICD\Model\ProjectQuaCICDModel;

use Kanboard\Event\TaskEvent;
use Kanboard\Model\TaskModel;
use Kanboard\Model\TaskFinderModel;
use Kanboard\Model\TaskCreationModel;
use Kanboard\Model\ProjectModel;
use Kanboard\Model\ColumnModel;
use Kanboard\Plugin\QuaCICD\Helper\HelperQuaCICD;
use Kanboard\Model\UserModel;

class ProductionActionTest extends Base
{
    public function setUp()
    {
        parent::setUp();
        Fixtures::deleteTestEnvironments();
        $plugin = new Loader($this->container);
        $plugin->scan();
    }

    public function tearDown()
    {
        parent::tearDown();

        Fixtures::deleteTestEnvironments();
    }

    public function testSucess()
    {      
        $projectModel = new ProjectModel($this->container);
        $columnModel = new ColumnModel($this->container);
        $taskCreationModel = new TaskCreationModel($this->container);
        $taskFinderModel = new TaskFinderModel($this->container);
        $productionAction = new ProductionAction($this->container);
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $userModel = new UserModel($this->container);

        $userUsername = Fixtures::randomDataString();
        $userModel->create(array('id' => '2', 'username' => $userUsername));

        $this->assertEquals(1, $projectModel->create(array('name' => 'test1'), 2));

        $values = Fixtures::generateValuesActions();

        $this->assertNotFalse($projectQuaCICDModel->create($values));
        exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh " . HelperQuaCICD::getCreateQuaCICDProjectParameters($userUsername, 1, HelperQuaCICD::formatProjectName($projectModel->getById(1)['name'])) . " 2>&1", $output, $return);
        $this->assertEquals(1, $taskCreationModel->create(array('project_id' => 1, 'title' => 'test', 'position' => 5)));


        $this->assertEquals(5,$columnModel->create(1, "Qualification", 0, ''));
        $this->assertEquals(6,$columnModel->create(1, "Production", 0, ''));

        $event = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 6,
            'src_column_id' => 5,
        ));

        $productionAction->setProjectId(1);
        $productionAction->setParam('column_id', 6);

        $this->assertTrue($productionAction->execute($event, TaskModel::EVENT_MOVE_COLUMN));

        $task = $taskFinderModel->getById(1);
        $this->assertNotEmpty($task);
    }

    public function testNoNewColumn()
    {
        $projectModel = new ProjectModel($this->container);
        $columnModel = new ColumnModel($this->container);
        $taskCreationModel = new TaskCreationModel($this->container);
        $taskFinderModel = new TaskFinderModel($this->container);
        $productionAction = new ProductionAction($this->container);
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);

        $this->assertEquals(1, $projectModel->create(array('name' => 'test1')));

        $values = Fixtures::generateValuesActions();

        $this->assertNotFalse($projectQuaCICDModel->create($values));

        $this->assertEquals(1, $taskCreationModel->create(array('project_id' => 1, 'title' => 'test', 'position' => 5)));

        $event = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 6,
            'src_column_id' => 5,
        ));

        $productionAction->setProjectId(1);

        $this->assertFalse($productionAction->execute($event, TaskModel::EVENT_MOVE_COLUMN));
    }

    public function testNotMovingInProductionColumn()
    {      
        $projectModel = new ProjectModel($this->container);
        $columnModel = new ColumnModel($this->container);
        $taskCreationModel = new TaskCreationModel($this->container);
        $taskFinderModel = new TaskFinderModel($this->container);
        $productionAction = new ProductionAction($this->container);
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);

        $this->assertEquals(1, $projectModel->create(array('name' => 'test1')));

        $values = Fixtures::generateValuesActions();

        $this->assertNotFalse($projectQuaCICDModel->create($values));

        $this->assertEquals(1, $taskCreationModel->create(array('project_id' => 1, 'title' => 'test', 'position' => 1)));


        $this->assertEquals(5,$columnModel->create(1, "WrongQualificationName", 0, ''));
        $this->assertEquals(6,$columnModel->create(1, "WrongProductionName", 0, ''));

        $event = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 2,
            'src_column_id' => 1,
        ));

        $productionAction->setProjectId(1);

        $this->assertFalse($productionAction->execute($event, TaskModel::EVENT_MOVE_COLUMN));
    }
    public function testScriptError()
    {   
        $projectModel = new ProjectModel($this->container);
        $columnModel = new ColumnModel($this->container);
        $taskCreationModel = new TaskCreationModel($this->container);
        $taskFinderModel = new TaskFinderModel($this->container);
        $productionAction = new ProductionAction($this->container);
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $userModel = new UserModel($this->container);

        $userUsername = 'testUser';
        $userModel->create(array('id' => '2', 'username' => $userUsername));

        $this->assertEquals(1, $projectModel->create(array('name' => 'test1'), 2));

        $values = Fixtures::generateValuesActions();

        $this->assertNotFalse($projectQuaCICDModel->create($values));
        exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh ". HelperQuaCICD::getCreateQuaCICDProjectParameters($userUsername, 1, HelperQuaCICD::formatProjectName($projectModel->getById(1)['name'])) . " 2>&1", $output, $return);
        $this->assertEquals(1, $taskCreationModel->create(array('project_id' => 1, 'title' => 'test', 'position' => 5)));


        $this->assertEquals(5,$columnModel->create(1, "Qualification", 0, ''));
        $this->assertEquals(6,$columnModel->create(1, "Production", 0, ''));

        $event = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 6,
            'src_column_id' => 5,
        ));

        $productionAction->setProjectId(1);
        $productionAction->setParam('column_id', 6);

        $scriptPath = HelperQuaCICD::getPathScripts($userUsername, 1, $projectModel->getById(1)['name']) . "Deploy_in_production.sh";
        file_put_contents($scriptPath, "echoa 'ERROR'");
        $this->assertFalse($productionAction->execute($event, TaskModel::EVENT_MOVE_COLUMN));

        $task = $taskFinderModel->getById(1);
        $this->assertSame('5', $task['column_id']);
    }

    public function testDefaultScriptWellExecuted()
    {    
        $projectModel = new ProjectModel($this->container);
        $columnModel = new ColumnModel($this->container);
        $taskCreationModel = new TaskCreationModel($this->container);
        $taskFinderModel = new TaskFinderModel($this->container);
        $qualificationAction = new QualificationAction($this->container);
        $productionAction = new ProductionAction($this->container);
        $branchCreationAction = new BranchCreationAction($this->container);
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $userModel = new UserModel($this->container);

        $userUsername = 'testUser';
        $userModel->create(array('id' => '2', 'username' => $userUsername));

        $this->assertEquals(1, $projectModel->create(Fixtures::generateValuesProjectModel(), 2));

        $values = Fixtures::generateValuesActions();

        $this->assertNotFalse($projectQuaCICDModel->create($values));   
        exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh ". HelperQuaCICD::getCreateQuaCICDProjectParameters($userUsername, 1, HelperQuaCICD::formatProjectName($projectModel->getById(1)['name'])) . " 2>&1", $output, $return);
        $this->assertEquals(1, $taskCreationModel->create(array('project_id' => 1, 'title' => 'test')));

        $this->assertEquals(5,$columnModel->create(1, "Qualification", 0, ''));
        $this->assertEquals(6,$columnModel->create(1, "Production", 0, ''));

        $event1 = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 3,
            'src_column_id' => 1,
        ));

        $branchCreationAction->setProjectId(1);
        $branchCreationAction->setParam('column_id', 3);
        $this->assertTrue($branchCreationAction->execute($event1, TaskModel::EVENT_MOVE_COLUMN));

        $event2 = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 5,
            'src_column_id' => 3,
        ));

        $qualificationAction->setProjectId(1);
        $qualificationAction->setParam('column_id', 5);

        $tempFolder = "/srv/" . QUACICD_SUBDIR_ENV . "/tempFolder"; 
        if (file_exists($tempFolder)) { unlink($tempFolder); }
        mkdir($tempFolder);
        if (is_dir($tempFolder)) {
            chdir($tempFolder);
            exec('git clone ' . HelperQuaCICD::getPathGitRepository($userUsername, 1 , $projectModel->getById(1)['name']) . " Development_Environment". " 2>&1", $output7, $return);
            chdir($tempFolder. "/Development_Environment");
            exec('git config user.email "dev@locahost"'. " 2>&1", $output7, $return);
            exec('git config user.name "Dossier Dev"'. " 2>&1", $output7, $return);
            exec('git checkout Task/1-test 2>&1', $output7, $return);
            exec('touch index.html'. " 2>&1", $output7, $return);            
            exec('git add .'. " 2>&1", $output7, $return);
            exec('git commit -m "Test Commit"'. " 2>&1", $output7, $return);
            exec('git push'. " 2>&1", $output7, $return);
            chdir("/srv");
            Fixtures::delete_files( $tempFolder . '/');
        }

        $this->assertTrue($qualificationAction->execute($event2, TaskModel::EVENT_MOVE_COLUMN));

        $event3 = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 6,
            'src_column_id' => 5,
        ));

        $productionAction->setProjectId(1);
        $productionAction->setParam('column_id', 6);

        $this->assertTrue($productionAction->execute($event3, TaskModel::EVENT_MOVE_COLUMN));

        $this->assertTrue(file_exists(HelperQuaCICD::getPathEnvironment($userUsername, 1, $projectModel->getById(1)['name']) . "/Production_Environment/index.html"));
    }

    public function testWithRenamingColumnName()
    {      
        $projectModel = new ProjectModel($this->container);
        $columnModel = new ColumnModel($this->container);
        $taskCreationModel = new TaskCreationModel($this->container);
        $taskFinderModel = new TaskFinderModel($this->container);
        $productionAction = new ProductionAction($this->container);
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $userModel = new UserModel($this->container);

        $userUsername = Fixtures::randomDataString();
        $userModel->create(array('id' => '2', 'username' => $userUsername));

        $this->assertEquals(1, $projectModel->create(array('name' => 'test1'), 2));

        $values = Fixtures::generateValuesActions();

        $this->assertNotFalse($projectQuaCICDModel->create($values));
        exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh " . HelperQuaCICD::getCreateQuaCICDProjectParameters($userUsername, 1, HelperQuaCICD::formatProjectName($projectModel->getById(1)['name'])) . " 2>&1", $output, $return);
        $this->assertEquals(1, $taskCreationModel->create(array('project_id' => 1, 'title' => 'test', 'position' => 5)));


        $this->assertEquals(5,$columnModel->create(1, "Qualification", 0, ''));
        $this->assertEquals(6,$columnModel->create(1, "Production", 0, ''));

        $columnModel->update($columnModel->getById(6)['id'], 'Another Name');

        $event = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 6,
            'src_column_id' => 5,
        ));

        $productionAction->setProjectId(1);
        $productionAction->setParam('column_id', 6);

        $this->assertTrue($productionAction->execute($event, TaskModel::EVENT_MOVE_COLUMN));

        $task = $taskFinderModel->getById(1);
        $this->assertNotEmpty($task);
    }
 }
