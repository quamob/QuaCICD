<?php

require_once 'tests/units/Base.php';
require_once 'plugins/QuaCICD/vendor/autoload.php';
require_once 'plugins/QuaCICD/Test/_helper/Fixtures.php';

use Kanboard\Plugin\QuaCICD\Plugin;
use Kanboard\Core\Plugin\Loader;
use Kanboard\Plugin\QuaCICD\Action\BranchCreationAction;
use Kanboard\Plugin\QuaCICD\Model\ProjectQuaCICDModel;

use Kanboard\Plugin\QuaBDD\Model\TaskQuaBDDGherkinModel;

use Kanboard\Event\TaskEvent;
use Kanboard\Model\TaskModel;
use Kanboard\Model\TaskFinderModel;
use Kanboard\Model\TaskCreationModel;
use Kanboard\Model\CommentModel;
use Kanboard\Model\ProjectModel;
use Kanboard\Model\ColumnModel;
use Kanboard\Plugin\QuaCICD\Helper\HelperQuaCICD;
use Kanboard\Model\UserModel;

class BranchCreationActionTest extends Base
{
    public function setUp()
    {
        parent::setUp();
        Fixtures::deleteTestEnvironments();
        $plugin = new Loader($this->container);
        $plugin->scan();
    }

    public function tearDown()
    {
        parent::tearDown();

        Fixtures::deleteTestEnvironments();
    }

    public function testSucess()
    {   
        $projectModel = new ProjectModel($this->container);
        $columnModel = new ColumnModel($this->container);
        $taskCreationModel = new TaskCreationModel($this->container);
        $taskFinderModel = new TaskFinderModel($this->container);
        $branchCreationAction = new BranchCreationAction($this->container);
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $userModel = new UserModel($this->container);

        $userUsername = Fixtures::randomDataString();
        $userModel->create(array('id' => '2', 'username' => $userUsername));

        $this->assertEquals(1, $projectModel->create(Fixtures::generateValuesProjectModel(), 2));
        $values = Fixtures::generateValuesActions();
        
        $this->assertNotFalse($projectQuaCICDModel->create($values));    
        exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh ". HelperQuaCICD::getCreateQuaCICDProjectParameters($userUsername, 1, HelperQuaCICD::formatProjectName($projectModel->getById(1)['name'])) .  " 2>&1", $output, $return);
        $this->assertEquals(1, $taskCreationModel->create(array('project_id' => 1, 'title' => Fixtures::randomDataString())));


        $this->assertEquals(5,$columnModel->create(1, "Qualification", 0, ''));
        $this->assertEquals(6,$columnModel->create(1, "Production", 0, ''));

        $event = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 3,
            'src_column_id' => 1,
        ));

        $branchCreationAction->setProjectId(1);
        $branchCreationAction->setParam('column_id', 3);

        $this->assertTrue($branchCreationAction->execute($event, TaskModel::EVENT_MOVE_COLUMN));

        $task = $taskFinderModel->getById(1);
        $this->assertNotEmpty($task);

        $allBranches = scandir(HelperQuaCICD::getPathGitRepository($userUsername, 1 , $projectModel->getById(1)['name']) . "/refs/heads/Task");
        $this->assertTrue(in_array("1-" . HelperQuaCICD::formatTaskTitle($taskFinderModel->getById(1)['title']), $allBranches));
    }

    public function testComments()
    {      
        $projectModel = new ProjectModel($this->container);
        $columnModel = new ColumnModel($this->container);
        $taskCreationModel = new TaskCreationModel($this->container);
        $taskFinderModel = new TaskFinderModel($this->container);
        $commentModel = new CommentModel($this->container);
        $branchCreationAction = new BranchCreationAction($this->container);
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $userModel = new UserModel($this->container);

        $userUsername = Fixtures::randomDataString();
        $userModel->create(array('id' => '2', 'username' => $userUsername));

        $this->assertEquals(1, $projectModel->create(Fixtures::generateValuesProjectModel(), 2));

        $values = Fixtures::generateValuesActions();
        
        $this->assertNotFalse($projectQuaCICDModel->create($values));    
        exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh " . HelperQuaCICD::getCreateQuaCICDProjectParameters($userUsername, 1, HelperQuaCICD::formatProjectName($projectModel->getById(1)['name'])) . " 2>&1", $output, $return);
        $this->assertEquals(1, $taskCreationModel->create(array('project_id' => 1, 'title' => Fixtures::randomDataString())));


        $this->assertEquals(5,$columnModel->create(1, "Qualification", 0, ''));
        $this->assertEquals(6,$columnModel->create(1, "Production", 0, ''));

        $event = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 3,
            'src_column_id' => 1,
        ));

        $branchCreationAction->setProjectId(1);
        $branchCreationAction->setParam('column_id', 3);

        $this->assertTrue($branchCreationAction->execute($event, TaskModel::EVENT_MOVE_COLUMN));

        $task = $taskFinderModel->getById(1);
        $comments = $commentModel->getAll(1);
        
        $this->assertSame("Git branch Task/1-" . HelperQuaCICD::formatTaskTitle($task['title']) . " successfully created", $comments[0]['comment']);
    }

    public function testBranchAlreadyExist()
    {      
        $projectModel = new ProjectModel($this->container);
        $columnModel = new ColumnModel($this->container);
        $taskCreationModel = new TaskCreationModel($this->container);
        $taskFinderModel = new TaskFinderModel($this->container);
        $commentModel = new CommentModel($this->container);
        $branchCreationAction = new BranchCreationAction($this->container);
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $userModel = new UserModel($this->container);

        $userUsername = Fixtures::randomDataString();
        $userModel->create(array('id' => '2', 'username' => $userUsername));

        $this->assertEquals(1, $projectModel->create(Fixtures::generateValuesProjectModel(), 2));

        $values = Fixtures::generateValuesActions();
        
        $this->assertNotFalse($projectQuaCICDModel->create($values));    
        exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh " . HelperQuaCICD::getCreateQuaCICDProjectParameters($userUsername, 1, HelperQuaCICD::formatProjectName($projectModel->getById(1)['name'])) . " 2>&1", $output, $return);
        $this->assertEquals(1, $taskCreationModel->create(array('project_id' => 1, 'title' => Fixtures::randomDataString())));
        $task = $taskFinderModel->getById(1);

        $this->assertEquals(5,$columnModel->create(1, "Qualification", 0, ''));
        $this->assertEquals(6,$columnModel->create(1, "Production", 0, ''));

        $event = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 3,
            'src_column_id' => 1,
        ));

        $branchCreationAction->setProjectId(1);
        $branchCreationAction->setParam('column_id', 3);

        $tempfile=tempnam(sys_get_temp_dir(),'');
        if (file_exists($tempfile)) { unlink($tempfile); }
        mkdir($tempfile);
        if (is_dir($tempfile)) {
            chdir($tempfile);
            exec('git clone ' . HelperQuaCICD::getPathGitRepository($userUsername, 1 , $projectModel->getById(1)['name']) . " Development_Environment". " 2>&1", $output, $return);
            chdir($tempfile. "/Development_Environment");
            exec('git checkout master 2>&1', $output, $return);
            exec('git checkout -b "' . "Task/1-"  . HelperQuaCICD::formatTaskTitle($task['title']) .'"'. " 2>&1", $output, $return);
            exec('git push --set-upstream origin "'. "Task/1-"  . HelperQuaCICD::formatTaskTitle($task['title']) . '"'. " 2>&1", $output, $return);
            exec('git branch -a', $branch);
            Fixtures::delete_files($tempfile . '/');
        }

        $this->assertTrue($branchCreationAction->execute($event, TaskModel::EVENT_MOVE_COLUMN));

        $comments = $commentModel->getAll(1);
        
        $this->assertSame("Git branch Task/1-" . HelperQuaCICD::formatTaskTitle($task['title']) . " already exist", $comments[0]['comment']);
    }

    public function testWithRenamingColumnName()
    {   
        $projectModel = new ProjectModel($this->container);
        $columnModel = new ColumnModel($this->container);
        $taskCreationModel = new TaskCreationModel($this->container);
        $taskFinderModel = new TaskFinderModel($this->container);
        $branchCreationAction = new BranchCreationAction($this->container);
        $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
        $userModel = new UserModel($this->container);

        $userUsername = Fixtures::randomDataString();
        $userModel->create(array('id' => '2', 'username' => $userUsername));

        $this->assertEquals(1, $projectModel->create(Fixtures::generateValuesProjectModel(), 2));
        $values = Fixtures::generateValuesActions();
        
        $this->assertNotFalse($projectQuaCICDModel->create($values));    
        exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh ". HelperQuaCICD::getCreateQuaCICDProjectParameters($userUsername, 1, HelperQuaCICD::formatProjectName($projectModel->getById(1)['name'])) .  " 2>&1", $output, $return);
        $this->assertEquals(1, $taskCreationModel->create(array('project_id' => 1, 'title' => Fixtures::randomDataString())));


        $this->assertEquals(5,$columnModel->create(1, "Qualification", 0, ''));
        $this->assertEquals(6,$columnModel->create(1, "Production", 0, ''));

        $columnModel->update($columnModel->getById(3)['id'], 'Another Name');

        $event = new TaskEvent(array(
            'task_id' => 1,
            'project_id' => 1,
            'column_id' => 3,
            'src_column_id' => 1,
        ));

        $branchCreationAction->setProjectId(1);
        $branchCreationAction->setParam('column_id', 3);

        $this->assertTrue($branchCreationAction->execute($event, TaskModel::EVENT_MOVE_COLUMN));

        $task = $taskFinderModel->getById(1);
        $this->assertNotEmpty($task);

        $allBranches = scandir(HelperQuaCICD::getPathGitRepository($userUsername, 1 , $projectModel->getById(1)['name']) . "/refs/heads/Task");
        $this->assertTrue(in_array("1-" . HelperQuaCICD::formatTaskTitle($taskFinderModel->getById(1)['title']), $allBranches));
    }

    public function testWithPluginQuaBDD()
    {   
        if(HelperQuaCICD::scanTestQuaBDDQuaCICD()){
            $projectModel = new ProjectModel($this->container);
            $columnModel = new ColumnModel($this->container);
            $taskCreationModel = new TaskCreationModel($this->container);
            $taskFinderModel = new TaskFinderModel($this->container);
            $branchCreationAction = new BranchCreationAction($this->container);
            $projectQuaCICDModel = new ProjectQuaCICDModel($this->container);
            $userModel = new UserModel($this->container);

            $userUsername = Fixtures::randomDataString();
            $userModel->create(array('id' => '2', 'username' => $userUsername));

            $this->assertEquals(1, $projectModel->create(Fixtures::generateValuesProjectModel(), 2));
            $values = Fixtures::generateValuesActions();
            
            $this->assertNotFalse($projectQuaCICDModel->create($values));    
            exec("plugins/QuaCICD/Script/Create_quaCICD_project.sh ". HelperQuaCICD::getCreateQuaCICDProjectParameters($userUsername, 1, HelperQuaCICD::formatProjectName($projectModel->getById(1)['name'])) .  " 2>&1", $output, $return);
            $this->assertEquals(1, $taskCreationModel->create(array('project_id' => 1, 'title' => 'test')));

            $taskGherkinModel = new TaskQuaBDDGherkinModel($this->container);

            $valuesGherkin = [
                'task_id' => '1',
                'title' => 'titleTest',
                'given' => json_encode(array('given1', 'given2', 'given3')),
                'when' => json_encode(array('when1', 'when2', 'when3')),
                'then' => json_encode(array('then1', 'then2', 'then3')),
            ];

            $taskGherkinModel->create($valuesGherkin);

            $this->assertEquals(5,$columnModel->create(1, "Qualification", 0, ''));
            $this->assertEquals(6,$columnModel->create(1, "Production", 0, ''));

            $event = new TaskEvent(array(
                'task_id' => 1,
                'project_id' => 1,
                'column_id' => 3,
                'src_column_id' => 1,
            ));

            $branchCreationAction->setProjectId(1);
            $branchCreationAction->setParam('column_id', 3);

            $this->assertTrue($branchCreationAction->execute($event, TaskModel::EVENT_MOVE_COLUMN));

            $tempFolderForbranchCreation = "/srv/" . QUACICD_SUBDIR_ENV . "/TestAddingFeaturesFile";
            if (file_exists($tempFolderForbranchCreation)) { unlink($tempFolderForbranchCreation); }
            mkdir($tempFolderForbranchCreation);
            if (is_dir($tempFolderForbranchCreation)) {
                chdir($tempFolderForbranchCreation);
                exec('git clone ' . HelperQuaCICD::getPathGitRepository($userUsername, 1 , $projectModel->getById(1)['name']) . " Development_Environment". " 2>&1", $output, $return);
                chdir($tempFolderForbranchCreation. "/Development_Environment");
                exec('git config user.email "dev@locahost"'. " 2>&1", $output, $return);
                exec('git config user.name "Dossier Dev"'. " 2>&1", $output, $return);
                exec('git checkout Task/1-test 2>&1', $output, $return);
                $this->assertTrue(file_exists($tempFolderForbranchCreation. '/Development_Environment/features/' . HelperQuaCICD::formatQuaBDDFeatureFile('test') . ".feature"));
                $this->assertTrue(file_exists($tempFolderForbranchCreation. '/Development_Environment/features/steps/' . HelperQuaCICD::formatQuaBDDFeatureFile('test') . "_step.py"));
                chdir("/srv");
                Fixtures::delete_files( $tempFolderForbranchCreation . '/');
            }
        }
        else{
            $this->assertTrue(true);
        }
        
    }

}

?>
