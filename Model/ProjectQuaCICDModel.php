<?php

namespace Kanboard\Plugin\QuaCICD\Model;

use Kanboard\Core\Base;

/**
 * ProjectQuaCICDModel
 *
 * @package  Kanboard\Plugin\QuaCICD\Model
 * @author   Alexandre Turpin
 */
class ProjectQuaCICDModel extends Base
{
    /**
     * SQL table name
     *
     * @var string
     */
    const TABLE = 'quaCICD_project';

    /**
     * Get a quaCICD project by the project id
     *
     * @access public
     * @param  integer   $project_id    Project id
     * @return array
     */
    public function getByProjectId($project_id)
    {
        return $this->db->table(self::TABLE)->eq('project_id', $project_id)->findOne();
    }

    /**
     * Get a quaCICD project by id
     *
     * @access public
     * @param  integer   $project_id    Project id
     * @return array
     */
    public function getById($id)
    {
        return $this->db->table(self::TABLE)->eq('id', $id)->findOne();
    }

    /**
     * Get all quaCICD projects
     *
     * @access public
     * @return array
     */
    public function getAll()
    {
        return $this->db->table(self::TABLE)->findAll();
    }

    /**
     * Get all quaCICD projects with given Ids
     *
     * @access public
     * @param  integer[]   $ids
     * @return array
     */
    public function getAllByIds(array $ids)
    {
        if (empty($ids)) {
            return array();
        }

        return $this->db->table(self::TABLE)->in('id', $ids)->findAll();
    }

    /**
     * Get all quaCICD project ids
     *
     * @access public
     * @return array
     */
    public function getAllIds()
    {
        return $this->db->table(self::TABLE)->findAllByColumn('id');
    }

    /**
     * Create a quaCICD project
     *
     * @access public
     * @param  array   $values Form values
     * @return int     Project id
     */
    public function create(array $values)
    {
        $this->db->startTransaction();

        if (! $this->db->table(self::TABLE)->save($values)) {
            $this->db->cancelTransaction();
            return false;
        }

        $projectQuaCICD_id = $this->db->getLastId();

        $this->db->closeTransaction();

        return (int) $projectQuaCICD_id;
    }

    /**
     * Return true if the quaCICD project exists
     *
     * @access public
     * @param  integer    $projectQuaCICD_id   QuaCICD Project id
     * @return boolean
     */
    public function exists($projectQuaCICD_id)
    {
        return $this->db->table(self::TABLE)->eq('id', $projectQuaCICD_id)->exists();
    }

    /**
     * Change the value of the qualification_script
     *
     * @access public
     * @param  integer   $projectQuaCICD_id    QuaCICD Project id
     * @param string    $QualificationScript  Path of the qualification script
     * @return bool
     */
    public function changeQualificationScript($projectQuaCICD_id, $qualificationScript)
    {
        return $this->exists($projectQuaCICD_id) &&
               $this->db
                    ->table(self::TABLE)
                    ->eq('id', $projectQuaCICD_id)
                    ->update(array('qualification_script' => $qualificationScript));
    }

    /**
     * Change the value of the production_script
     *
     * @access public
     * @param  integer   $projectQuaCICD_id    QuaCICD Project id
     * @param string    $productionScript   Path of the production script
     * @return bool
     */
    public function changeProductionScript($projectQuaCICD_id, $productionScript)
    {
        return $this->exists($projectQuaCICD_id) &&
               $this->db
                    ->table(self::TABLE)
                    ->eq('id', $projectQuaCICD_id)
                    ->update(array('production_script' => $productionScript));
    }
}