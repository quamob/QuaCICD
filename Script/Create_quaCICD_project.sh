#
#   This script creates all repositories we need for a quaCICD project
#

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

git --version 2>&1 >/dev/null
GIT_IS_AVAILABLE=$?

if [ ! $GIT_IS_AVAILABLE -eq 0 ]; then
    echo ' You need git installed in order to create QuaCICD project. More informations in the requirements section in Readme (https://gitlab.com/quamob/QuaCICD/blob/master/README.md).'
    exit 1
fi

if [ -w '/var/www/html' ] && [ -w '/srv/' ]; then 
    mkdir -m 777 -p $2
    mkdir -m 777 -p $3
    mkdir -m 777 -p $1
else 
    if [ -w '/var/www/html' ] ; then
        echo "You don't have permission to write in the folder /srv. More informations in the requirements section in Readme (https://gitlab.com/quamob/QuaCICD/blob/master/README.md)."
    else
        echo "You don't have permission to write in the folder /var/www/html. More informations in the requirements section in Readme (https://gitlab.com/quamob/QuaCICD/blob/master/README.md)."
    fi
    exit 1
fi

cp -p $SCRIPTPATH/Deploy_in_qualification.sh $1
cp -p $SCRIPTPATH/Deploy_in_production.sh $1
cp -p $SCRIPTPATH/Merge_Branch.sh $1

if [ -d "$3" ]; then
    cd $3
    git init --bare
    if [ -d "$2" ]; then
        cd $2
        git clone $3 Development_Environment
        cd  Development_Environment
        
        git config user.email "dev@locahost"
        git config user.name "Dev Folder" 
        
        touch Readme.md
        git add .
        git commit -m "First commit in master"
        git push
        git checkout -b qualification
        git push --set-upstream origin qualification

        cd ..
        rm -rf Development_Environment

        git clone $3 --branch qualification --single-branch Qualification_Environment
        git clone $3 --branch master --single-branch Production_Environment
    fi
fi

chmod -R a+w $1
chmod -R a+w $2
chmod -R a+w $3