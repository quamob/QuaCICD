#
#   This script merge branch into another branch
#

checkConflict()
{
    NB_CONFLICTS=$(git ls-files -u | wc -l)
    if [ "$NB_CONFLICTS" -gt 0 ] ; then
        echo "There is a merge conflict.  Check comments to resolve it."
        echo $(git diff --name-only)
        ERROR=1
        return 1
    fi

    return 0
}

GIT_REPOSITORY=$1
ENVIRONMENT_PATH=$2
BRANCH_TO_MERGE=$3
SQUASH_OPTION=$4
BRANCH_TO_MERGE_INTO=$5

ERROR=0;



cd $ENVIRONMENT_PATH
git clone $GIT_REPOSITORY tempFolderForMerge
chmod -R a+w tempFolderForMerge
cd tempFolderForMerge
    
git config user.email "dev@locahost"
git config user.name "Dossier Dev"

git checkout $BRANCH_TO_MERGE

git merge origin/$BRANCH_TO_MERGE_INTO

if checkConflict; then
    git push
    git checkout $BRANCH_TO_MERGE_INTO

    if [ $SQUASH_OPTION = true ]; then
        git merge $BRANCH_TO_MERGE --squash
    else
        git merge $BRANCH_TO_MERGE
    fi

    if checkConflict; then
        if [ $SQUASH_OPTION = true ]; then
            git commit -m "$BRANCH_TO_MERGE"
        fi
        git push
    fi
fi

cd $ENVIRONMENT_PATH
rm -rf tempFolderForMerge

if [ $ERROR = '1' ]; then
    exit 1
fi